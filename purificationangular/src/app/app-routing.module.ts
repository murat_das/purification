import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { DeviceComponent } from './components/device/device.component';
import { TestResultComponent } from './components/testResult/testResult.component';
import { UnitComponent } from './components/unit/unit.component';
import { SensorComponent } from './components/sensor/sensor.component';
import { EngineElementComponent } from './components/engineElement/engineElement.component';
import { ElementStatusComponent } from './components/elementStatus/elementStatus.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SensorDataReportComponent } from './components/dashboard/sensorDataReport/sensorDataReport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'update-password', component: ChangePasswordComponent },
                    { path: 'devices', component: DeviceComponent,  canActivate: [AppRouteGuard] },
                    { path: 'units', component: UnitComponent,  canActivate: [AppRouteGuard] },
                    { path: 'sensors', component: SensorComponent,  canActivate: [AppRouteGuard] },
                    { path: 'testResults', component: TestResultComponent,  canActivate: [AppRouteGuard] },
                    { path: 'engineElements', component: EngineElementComponent,  canActivate: [AppRouteGuard] },
                    { path: 'elementStatuses', component: ElementStatusComponent,  canActivate: [AppRouteGuard] },
                    { path: 'profiles', component: ProfileComponent,  canActivate: [AppRouteGuard] },
                    { path: 'sensorDataReports', component: SensorDataReportComponent,  canActivate: [AppRouteGuard] },

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
