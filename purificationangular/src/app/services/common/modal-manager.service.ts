import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { CreateDeviceDialogComponent } from "@app/components/device/create-device/create-device-dialog.component";
import { EditDeviceDialogComponent } from "@app/components/device/edit-device/edit-device-dialog.component";
import { GetDeviceInput } from "../device/dtos/GetDeviceInput";
import { CreateUnitDialogComponent } from "@app/components/unit/create-unit/create-unit-dialog.component";
import { GetUnitInput } from "../unit/dtos/GetUnitInput";
import { EditUnitDialogComponent } from "@app/components/unit/edit-unit/edit-unit-dialog.component";
import { CreateSensorDialogComponent } from "@app/components/sensor/create-sensor/create-sensor-dialog.component";
import { GetSensorInput } from "../sensor/dtos/GetSensorInput";
import { EditSensorDialogComponent } from "@app/components/sensor/edit-sensor/edit-sensor-dialog.component";
import { CreateTestResultDialogComponent } from "@app/components/testResult/create-testResult/create-testResult-dialog.component";
import { GetTestResultInput } from "../testResult/dtos/GetTestResultInput";
import { EditTestResultDialogComponent } from "@app/components/testResult/edit-testResult/edit-testResult-dialog.component";
import { GetEngineElementInput } from "../engineElement/dtos/GetEngineElementInput";
import { CreateEngineElementDialogComponent } from "@app/components/engineElement/create-engineElement/create-engineElement-dialog.component";
import { EditEngineElementDialogComponent } from "@app/components/engineElement/edit-engineElement/edit-engineElement-dialog.component";
import { CreateElementStatusDialogComponent } from "@app/components/elementStatus/create-elementStatus/create-elementStatus-dialog.component";
import { GetElementStatusInput } from "../elementStatus/dtos/GetElementStatusInput";
import { EditElementStatusDialogComponent } from "@app/components/elementStatus/edit-elementStatus/edit-elementStatus-dialog.component";

@Injectable({
  providedIn: "root",
})
export class ModalManagerService {
  constructor(private _dialog: MatDialog) {}

  // Devices
  openCreateDeviceDialog(parameters?: any): any {
    let createDeviceDialog;
    createDeviceDialog = this._dialog.open(CreateDeviceDialogComponent, {
      data: parameters,
    });

    return createDeviceDialog;
  }

  openEditDeviceDialog(id?: number): any {
    let editDeviceDialog;
    let getDeviceInput = new GetDeviceInput();
    getDeviceInput.id = id;

    editDeviceDialog = this._dialog.open(EditDeviceDialogComponent, {
      data: getDeviceInput,
    });

    return editDeviceDialog;
  }

  // Units
  openCreateUnitDialog(parameters?: any): any {
    let createUnitDialog;
    createUnitDialog = this._dialog.open(CreateUnitDialogComponent, {
      data: parameters,
    });

    return createUnitDialog;
  }

  openEditUnitDialog(id?: number): any {
    let editUnitDialog;
    let getUnitInput = new GetUnitInput();
    getUnitInput.id = id;

    editUnitDialog = this._dialog.open(EditUnitDialogComponent, {
      data: getUnitInput,
    });

    return editUnitDialog;
  }

  // Sensors
  openCreateSensorDialog(parameters?: any): any {
    let createSensorDialog;
    createSensorDialog = this._dialog.open(CreateSensorDialogComponent, {
      data: parameters,
    });

    return createSensorDialog;
  }

  openEditSensorDialog(id?: number): any {
    let editSensorDialog;
    let getSensorInput = new GetSensorInput();
    getSensorInput.id = id;

    editSensorDialog = this._dialog.open(EditSensorDialogComponent, {
      data: getSensorInput,
    });

    return editSensorDialog;
  }

   // TestResults
   openCreateTestResultDialog(parameters?: any): any {
    let createTestResultDialog;
    createTestResultDialog = this._dialog.open(CreateTestResultDialogComponent, {
      data: parameters,
    });

    return createTestResultDialog;
  }

  openEditTestResultDialog(id?: number): any {
    let editTestResultDialog;
    let getTestResultInput = new GetTestResultInput();
    getTestResultInput.id = id;

    editTestResultDialog = this._dialog.open(EditTestResultDialogComponent, {
      data: getTestResultInput,
    });

    return editTestResultDialog;
  }

  // EngineElements
  openCreateEngineElementDialog(parameters?: any): any {
    let createEngineElementDialog;
    createEngineElementDialog = this._dialog.open(CreateEngineElementDialogComponent, {
      data: parameters,
    });

    return createEngineElementDialog;
  }

  openEditEngineElementDialog(id?: number): any {
    let editEngineElementDialog;
    let getEngineElementInput = new GetEngineElementInput();
    getEngineElementInput.id = id;

    editEngineElementDialog = this._dialog.open(EditEngineElementDialogComponent, {
      data: getEngineElementInput,
    });

    return editEngineElementDialog;
  }

   // ElementStatuss
   openCreateElementStatusDialog(parameters?: any): any {
    let createElementStatusDialog;
    createElementStatusDialog = this._dialog.open(CreateElementStatusDialogComponent, {
      data: parameters,
    });

    return createElementStatusDialog;
  }

  openEditElementStatusDialog(id?: number): any {
    let editElementStatusDialog;
    let getElementStatusInput = new GetElementStatusInput();
    getElementStatusInput.id = id;

    editElementStatusDialog = this._dialog.open(EditElementStatusDialogComponent, {
      data: getElementStatusInput,
    });

    return editElementStatusDialog;
  }
}
