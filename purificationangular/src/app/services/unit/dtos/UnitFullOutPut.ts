import { isObject } from "util";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class UnitFullOutPut implements IUnitFullOutPut {
    id: number;
    name: string;
    tenant:TenantPartOutPut;
    
    constructor(data?: IUnitFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.tenant = data.tenant;
                
            }
        }
    }

    static fromJS(data: any): UnitFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new UnitFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["tenant"] = this.tenant;
        
        return data; 
    }

    clone(): UnitFullOutPut {
        const json = this.toJSON();
        let result = new UnitFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IUnitFullOutPut{
    id:number;
    name:string;
    tenant:TenantPartOutPut;
}
