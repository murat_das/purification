export class GetUnitInput implements IGetUnitInput {
    id: number;
}

export interface IGetUnitInput{
    id:number;
}
