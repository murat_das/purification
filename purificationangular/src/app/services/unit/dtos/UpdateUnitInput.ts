export class UpdateUnitInput implements IUpdateUnitInput {
    name:string;
    id: number;
}

export interface IUpdateUnitInput {
    name:string;
    id:number;
}
