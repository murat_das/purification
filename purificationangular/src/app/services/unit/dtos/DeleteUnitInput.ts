export class DeleteUnitInput implements IDeleteUnitInput {
    name: string;
    id: number;
}

export interface IDeleteUnitInput{
    name: string;
    id:number;
}
