import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class CreateUnitInput implements ICreateUnitInput {
    id: number;
    name: string;
    tenant: TenantPartOutPut;

}

export interface ICreateUnitInput{
    id:number;
    name:string;
    tenant: TenantPartOutPut;

}
