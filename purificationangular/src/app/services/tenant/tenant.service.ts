import { Injectable, Inject, Optional } from "@angular/core";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf,
} from "rxjs";
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
} from "rxjs/operators";
import { UtilsService } from "@app/services/common/utils.service";
import {
  HttpClient,
  HttpResponseBase,
  HttpResponse,
  HttpHeaders,
} from "@angular/common/http";
import { API_BASE_URL } from "@shared/service-proxies/service-proxies";
import { TenantPartOutPut } from "./dtos/TenantPartOutPut";

@Injectable({
  providedIn: "root",
})
export class TenantService {
  private utilsService: UtilsService;
  private http: HttpClient;
  private baseUrl: string;
  protected jsonParseReviver:
    | ((key: string, value: any) => any)
    | undefined = undefined;

  constructor(
    @Inject(HttpClient) http: HttpClient,
    utilsService: UtilsService,
    @Optional() @Inject(API_BASE_URL) baseUrl?: string
  ) {
    this.utilsService = utilsService;
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  
  getList(): Observable<TenantPartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Tenant/GetList";
    url_ = url_.replace(/[?&]$/, "");
    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetList(<any>response_);
            } catch (e) {
              return <Observable<TenantPartOutPut[]>>(<any>_observableThrow(e));
            }
          } else {
            return <Observable<TenantPartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
          }
        })
      );
  }

  protected processGetList(
    response: HttpResponseBase
  ): Observable<TenantPartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<TenantPartOutPut[]>(<any>null);
  }

 
}
