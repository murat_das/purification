export class TenantPartOutPut implements ITenantPartOutPut {
    id: number;
    name: string;
    tenancyName:string;

}


export interface ITenantPartOutPut{
    id:number;
    name:string;
    tenancyName:string;
}