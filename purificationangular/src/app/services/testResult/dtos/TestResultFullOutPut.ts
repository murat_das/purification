import { isObject } from "util";
import { SensorPartOutPut } from "@app/services/sensor/dtos/SensorPartOutPut";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class TestResultFullOutPut implements ITestResultFullOutPut {
    id: number;
    value: number;
    creationTime:Date;
    tenant:TenantPartOutPut;
    sensor: SensorPartOutPut;

    constructor(data?: ITestResultFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }
   

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.value = data.name;
                this.sensor = data.sensor;
                this.tenant = data.tenant;
                this.creationTime = data.creationTime;
                
            }
        }
    }

    static fromJS(data: any): TestResultFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new TestResultFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["value"] = this.value;
        data["sensor"] = this.sensor;
        data["tenant"] = this.tenant;
        data["creationTime"] = this.creationTime;

        
        return data; 
    }

    clone(): TestResultFullOutPut {
        const json = this.toJSON();
        let result = new TestResultFullOutPut();
        result.init(json);
        return result;
    }
}

export interface ITestResultFullOutPut{
    id:number;
    value:number;
    creationTime:Date;
    sensor:SensorPartOutPut;
    tenant:TenantPartOutPut;
}
