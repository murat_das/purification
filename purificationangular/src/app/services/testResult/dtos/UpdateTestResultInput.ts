import { SensorPartOutPut } from "@app/services/sensor/dtos/SensorPartOutPut";

export class UpdateTestResultInput implements IUpdateTestResultInput {
  id: number;
  value: number;
  sensor: SensorPartOutPut;
}

export interface IUpdateTestResultInput {
  id: number;
  value: number;
  sensor: SensorPartOutPut;
}
