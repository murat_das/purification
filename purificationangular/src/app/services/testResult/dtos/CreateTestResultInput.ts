import { SensorPartOutPut } from "@app/services/sensor/dtos/SensorPartOutPut";

export class CreateTestResultInput implements ICreateTestResultInput {
  value: number;
  sensor: SensorPartOutPut;
}

export interface ICreateTestResultInput {
  value: number;
  sensor: SensorPartOutPut;

}
