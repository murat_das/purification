export class TestResultPartOutPut implements ITestResultPartOutPut {
    id: number;
    value: number;

}


export interface ITestResultPartOutPut{
    id:number;
    value:number;
}