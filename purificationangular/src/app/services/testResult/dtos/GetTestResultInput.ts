export class GetTestResultInput implements IGetTestResultInput {
    id: number;
}

export interface IGetTestResultInput{
    id:number;
}
