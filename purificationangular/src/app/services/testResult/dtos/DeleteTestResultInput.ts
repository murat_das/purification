import { SensorPartOutPut } from "@app/services/sensor/dtos/SensorPartOutPut";

export class DeleteTestResultInput implements IDeleteTestResultInput {
  id: number;
  value: number;
  sensor: SensorPartOutPut;
}

export interface IDeleteTestResultInput {
  id: number;
  value: number;
  sensor: SensorPartOutPut;
}
