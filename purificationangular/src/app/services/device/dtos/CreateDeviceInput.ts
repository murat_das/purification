import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class CreateDeviceInput implements ICreateDeviceInput {
    tenant: TenantPartOutPut;
    id: number;
    name: string;
}

export interface ICreateDeviceInput{
    id:number;
    name:string;
    tenant:TenantPartOutPut;
}
