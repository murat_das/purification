export class DeleteDeviceInput implements IDeleteDeviceInput {
    name: string;
    id: number;
}

export interface IDeleteDeviceInput{
    name: string;
    id:number;
}
