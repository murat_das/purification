export class DevicePartOutPut implements IDevicePartOutPut {
    id: number;
    name: string;
}


export interface IDevicePartOutPut{
    id:number;
    name:string;
}