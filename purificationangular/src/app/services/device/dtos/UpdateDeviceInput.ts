export class UpdateDeviceInput implements IUpdateDeviceInput {
    name:string;
    id: number;
}

export interface IUpdateDeviceInput {
    name:string;
    id:number;
}
