import { isObject } from "util";
import { EngineElementFullOutPut } from "@app/services/engineElement/dtos/EngineElementFullOutPut";

export class ElementStatusFullOutPut implements IElementStatusFullOutPut {
    id:number;
    status:number;
    creationTime:Date;
    engineElement:EngineElementFullOutPut;

    constructor(data?: IElementStatusFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.status = data.status;
                this.engineElement = data.engineElement;
                this.creationTime = data.creationTime;
            }
        }
    }

    static fromJS(data: any): ElementStatusFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new ElementStatusFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["status"] = this.status;
        data["engineElement"] = this.engineElement;
        data["creationTime"] = this.creationTime;
        return data; 
    }

    clone(): ElementStatusFullOutPut {
        const json = this.toJSON();
        let result = new ElementStatusFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IElementStatusFullOutPut{
    id:number;
    status:number;
    creationTime:Date;
    engineElement:EngineElementFullOutPut;
}
