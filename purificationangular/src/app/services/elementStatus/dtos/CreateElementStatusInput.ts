import { EngineElementPartOutPut } from "@app/services/engineElement/dtos/EngineElementPartOutPut";

export class CreateElementStatusInput implements ICreateElementStatusInput {
  status: number;
  engineElement: EngineElementPartOutPut;

}

export interface ICreateElementStatusInput {
  status: number;
  engineElement: EngineElementPartOutPut;
}
