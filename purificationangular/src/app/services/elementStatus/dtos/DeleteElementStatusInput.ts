export class DeleteElementStatusInput implements IDeleteElementStatusInput {
    id: number;
  }
  
  export interface IDeleteElementStatusInput {
    id: number;
  }
  