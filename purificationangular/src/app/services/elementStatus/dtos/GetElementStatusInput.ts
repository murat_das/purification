export class GetElementStatusInput implements IGetElementStatusInput {
  id: number;
}

export interface IGetElementStatusInput {
  id: number;
}
