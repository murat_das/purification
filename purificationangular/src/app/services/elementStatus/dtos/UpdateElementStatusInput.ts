import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";

export class UpdateElementStatusInput implements IUpdateElementStatusInput {
    id:number;
    name: string;
    displayName: string;
    status: number;
    device: DeviceFullOutPut;
}

export interface IUpdateElementStatusInput {
    id:number;
    name: string;
    displayName: string;
    status: number;
    device: DeviceFullOutPut;
}
