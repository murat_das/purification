/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ElementStatusService } from './elementStatus.service';

describe('Service: ElementStatus', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElementStatusService]
    });
  });

  it('should ...', inject([ElementStatusService], (service: ElementStatusService) => {
    expect(service).toBeTruthy();
  }));
});
