import { Injectable, Inject, Optional } from "@angular/core";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf,
} from "rxjs";
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
} from "rxjs/operators";
import { UtilsService } from "@app/services/common/utils.service";
import {
  HttpClient,
  HttpResponseBase,
  HttpResponse,
  HttpHeaders,
} from "@angular/common/http";
import { API_BASE_URL } from "@shared/service-proxies/service-proxies";

import { ElementStatusFullOutPut } from "./dtos/ElementStatusFullOutPut";
import { CreateElementStatusInput } from "./dtos/CreateElementStatusInput";
import { GetElementStatusInput } from "./dtos/GetElementStatusInput";
import { UpdateElementStatusInput } from "./dtos/UpdateElementStatusInput";
import { DeleteElementStatusInput } from "./dtos/DeleteElementStatusInput";

@Injectable({
  providedIn: "root",
})
export class ElementStatusService {
  private utilsService: UtilsService;
  private http: HttpClient;
  private baseUrl: string;
  protected jsonParseReviver:
    | ((key: string, value: any) => any)
    | undefined = undefined;

  constructor(
    @Inject(HttpClient) http: HttpClient,
    utilsService: UtilsService,
    @Optional() @Inject(API_BASE_URL) baseUrl?: string
  ) {
    this.utilsService = utilsService;
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  /**
   * @param body (required)
   * @return Success
   */
  create(body: CreateElementStatusInput | undefined): Observable<ElementStatusFullOutPut> {
    let url_ = this.baseUrl + "/api/services/app/ElementStatus/Create";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processCreate(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processCreate(<any>response_);
            } catch (e) {
              return <Observable<ElementStatusFullOutPut>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<ElementStatusFullOutPut>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processCreate(
    response: HttpResponseBase
  ): Observable<ElementStatusFullOutPut> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = ElementStatusFullOutPut.fromJS(resultData200);
          return _observableOf(result200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<ElementStatusFullOutPut>(<any>null);
  }

  /**
   * @param body (required)
   * @return Success
   */
  get(body: GetElementStatusInput): Observable<ElementStatusFullOutPut> {
    let url_ = this.baseUrl + "/api/services/app/ElementStatus/Get";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGet(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGet(<any>response_);
            } catch (e) {
              return <Observable<ElementStatusFullOutPut>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<ElementStatusFullOutPut>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGet(
    response: HttpResponseBase
  ): Observable<ElementStatusFullOutPut> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = ElementStatusFullOutPut.fromJS(resultData200);
          return _observableOf(result200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<ElementStatusFullOutPut>(<any>null);
  }

  /**
   * @return Success
   */
  getList(): Observable<ElementStatusFullOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/ElementStatus/GetList";
    url_ = url_.replace(/[?&]$/, "");
    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetList(<any>response_);
            } catch (e) {
              return <Observable<ElementStatusFullOutPut[]>>(<any>_observableThrow(e));
            }
          } else {
            return <Observable<ElementStatusFullOutPut[]>>(
              (<any>_observableThrow(response_))
            );
          }
        })
      );
  }

  protected processGetList(
    response: HttpResponseBase
  ): Observable<ElementStatusFullOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<ElementStatusFullOutPut[]>(<any>null);
  }

  /**
   * @param body (required)
   * @return Success
   */
  delete(body: DeleteElementStatusInput): Observable<void> {
    let url_ = this.baseUrl + "/api/services/app/ElementStatus/Delete";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);
    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processDelete(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processDelete(<any>response_);
            } catch (e) {
              return <Observable<void>>(<any>_observableThrow(e));
            }
          } else return <Observable<void>>(<any>_observableThrow(response_));
        })
      );
  }

  protected processDelete(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return _observableOf<void>(<any>null);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<void>(<any>null);
  }

  /**
   * @param body (optional)
   * @return Success
   */
  update(body: UpdateElementStatusInput): Observable<ElementStatusFullOutPut> {
    let url_ = this.baseUrl + "/api/services/app/ElementStatus/Update";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processUpdate(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processUpdate(<any>response_);
            } catch (e) {
              return <Observable<ElementStatusFullOutPut>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<ElementStatusFullOutPut>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processUpdate(
    response: HttpResponseBase
  ): Observable<ElementStatusFullOutPut> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          result200 = ElementStatusFullOutPut.fromJS(resultData200);
          return _observableOf(result200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<ElementStatusFullOutPut>(<any>null);
  }
}
