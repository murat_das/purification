import { NgModule } from '@angular/core';
import { ModalManagerService } from './common/modal-manager.service';
import { UtilsService } from './common/utils.service';
import { UnitComponent } from '@app/components/unit/unit.component';
import { SensorComponent } from '@app/components/sensor/sensor.component';
import { DeviceComponent } from '@app/components/device/device.component';
import { EngineElementComponent } from '@app/components/engineElement/engineElement.component';
import { ElementStatusComponent } from '@app/components/elementStatus/elementStatus.component';
import { TestResultComponent } from '@app/components/testResult/testResult.component';
import { ProfileComponent } from '@app/components/profile/profile.component';
import { SensorDataReportComponent } from '@app/components/dashboard/sensorDataReport/sensorDataReport.component';

@NgModule({
  providers: [
    UtilsService,
    ModalManagerService,
    DeviceComponent,
    UnitComponent,
    SensorComponent,
    EngineElementComponent,
    ElementStatusComponent,
    TestResultComponent,
    ProfileComponent,
    SensorDataReportComponent,
  ]
})
export class PurificationServiceModule { }
