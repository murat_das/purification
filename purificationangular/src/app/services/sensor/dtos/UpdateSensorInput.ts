import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";

export class UpdateSensorInput implements IUpdateSensorInput {
    id:number;
    name: string;
    displayName: string;
    unit: UnitPartOutPut;
    // device: DeviceFullOutPut;
}

export interface IUpdateSensorInput {
    id:number;
    name: string;
    displayName: string;
    unit: UnitPartOutPut;
    // device: DeviceFullOutPut;
}
