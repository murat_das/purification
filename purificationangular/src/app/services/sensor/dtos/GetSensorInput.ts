export class GetSensorInput implements IGetSensorInput {
    id: number;
}

export interface IGetSensorInput{
    id:number;
}
