export class SensorPartOutPut implements ISensorPartOutPut {
    id: number;
    name: string;
    displayName:string;

}


export interface ISensorPartOutPut{
    id:number;
    name:string;
    displayName:string;
}