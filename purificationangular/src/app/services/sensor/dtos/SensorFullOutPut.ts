import { isObject } from "util";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class SensorFullOutPut implements ISensorFullOutPut {
    id: number;
    name: string;
    displayName: string;
    unit: UnitPartOutPut;
    tenant:TenantPartOutPut;
    device: DeviceFullOutPut;

    constructor(data?: ISensorFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.displayName = data.displayName;
                this.unit = data.unit;
                this.device = data.device;
                this.tenant = data.tenant;
            }
        }
    }

    static fromJS(data: any): SensorFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new SensorFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["unit"] = this.unit;
        data["device"] = this.device;
        data["tenant"] = this.tenant;
        return data; 
    }

    clone(): SensorFullOutPut {
        const json = this.toJSON();
        let result = new SensorFullOutPut();
        result.init(json);
        return result;
    }
}

export interface ISensorFullOutPut{
    id:number;
    name:string;
    displayName:string;
    tenant:TenantPartOutPut;
    unit:UnitPartOutPut;
    device:DeviceFullOutPut;
}
