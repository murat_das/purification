export class DeleteSensorInput implements IDeleteSensorInput {
  id: number;
  name: string;
  displayName: string;
}

export interface IDeleteSensorInput {
  id: number;
  name: string;
  displayName: string;
}
