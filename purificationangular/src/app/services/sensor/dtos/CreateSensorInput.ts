import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";

export class CreateSensorInput implements ICreateSensorInput {
  name: string;
  displayName: string;
  unit: UnitPartOutPut;
  device: DeviceFullOutPut;
}

export interface ICreateSensorInput {
  name: string;
  displayName: string;
  unit: UnitPartOutPut;
  device: DeviceFullOutPut;
}
