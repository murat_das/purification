/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EngineElementService } from './engineElement.service';

describe('Service: EngineElement', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngineElementService]
    });
  });

  it('should ...', inject([EngineElementService], (service: EngineElementService) => {
    expect(service).toBeTruthy();
  }));
});
