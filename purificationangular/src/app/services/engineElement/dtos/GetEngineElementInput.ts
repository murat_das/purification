export class GetEngineElementInput implements IGetEngineElementInput {
    id: number;
}

export interface IGetEngineElementInput{
    id:number;
}
