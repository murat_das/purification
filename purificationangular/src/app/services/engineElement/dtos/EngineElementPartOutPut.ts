export class EngineElementPartOutPut implements IEngineElementPartOutPut {
    id: number;
    name: string;
    displayName:string;

}


export interface IEngineElementPartOutPut{
    id:number;
    name:string;
    displayName:string;
}