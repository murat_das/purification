export class DeleteEngineElementInput implements IDeleteEngineElementInput {
  id: number;
  name: string;
  displayName: string;
}

export interface IDeleteEngineElementInput {
  id: number;
  name: string;
  displayName: string;
}
