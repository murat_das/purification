import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";

export class UpdateEngineElementInput implements IUpdateEngineElementInput {
  id: number;
  name: string;
  displayName: string;
  status: number;
  device: DeviceFullOutPut;
}

export interface IUpdateEngineElementInput {
  id: number;
  name: string;
  displayName: string;
  status: number;
  device: DeviceFullOutPut;
}
