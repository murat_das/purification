import { isObject } from "util";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

export class EngineElementFullOutPut implements IEngineElementFullOutPut {
    id: number;
    name: string;
    displayName: string;
    status:number;
    tenant:TenantPartOutPut;
    device: DeviceFullOutPut;

    constructor(data?: IEngineElementFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.displayName = data.displayName;
                this.status = data.status;
                this.device = data.device;
                this.tenant = data.tenant;
            }
        }
    }

    static fromJS(data: any): EngineElementFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new EngineElementFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["displayName"] = this.displayName;
        data["status"] = this.status;
        data["device"] = this.device;
        data["tenant"] = this.tenant;
        return data; 
    }

    clone(): EngineElementFullOutPut {
        const json = this.toJSON();
        let result = new EngineElementFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IEngineElementFullOutPut{
    id:number;
    name:string;
    displayName:string;
    status:number;
    tenant:TenantPartOutPut;
    device:DeviceFullOutPut;
}
