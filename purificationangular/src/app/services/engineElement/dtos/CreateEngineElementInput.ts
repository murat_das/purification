import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";

export class CreateEngineElementInput implements ICreateEngineElementInput {
  name: string;
  displayName: string;
  status: number;
  device: DeviceFullOutPut;
}

export interface ICreateEngineElementInput {
  name: string;
  displayName: string;
  status: number;
  device: DeviceFullOutPut;
}
