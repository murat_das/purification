import { Injectable, Inject, Optional } from "@angular/core";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf,
} from "rxjs";
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
} from "rxjs/operators";
import { UtilsService } from "@app/services/common/utils.service";
import {
  HttpClient,
  HttpResponseBase,
  HttpResponse,
  HttpHeaders,
} from "@angular/common/http";
import { API_BASE_URL } from "@shared/service-proxies/service-proxies";
import { SensorReportLineChartOutPut } from "./dtos/SensorReportLineChartOutPut";
import { GetSensorInput } from "../sensor/dtos/GetSensorInput";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  private utilsService: UtilsService;
  private http: HttpClient;
  private baseUrl: string;
  protected jsonParseReviver:
    | ((key: string, value: any) => any)
    | undefined = undefined;

  constructor(
    @Inject(HttpClient) http: HttpClient,
    utilsService: UtilsService,
    @Optional() @Inject(API_BASE_URL) baseUrl?: string
  ) {
    this.utilsService = utilsService;
    this.http = http;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

 

  /**
   * @param body (required)
   * @return Success
   */
  getSensorLineChartList(body: GetSensorInput): Observable<SensorReportLineChartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Dashboard/GetSensorLineChartList";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetSensorLineChartList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetSensorLineChartList(<any>response_);
            } catch (e) {
              return <Observable<SensorReportLineChartOutPut[]>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<SensorReportLineChartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGetSensorLineChartList(
    response: HttpResponseBase
  ): Observable<SensorReportLineChartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          // result200 = SensorReportLineChartOutPut.fromJS(resultData200);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<SensorReportLineChartOutPut[]>(<any>null);
  }

  
  /**
   * @param body (required)
   * @return Success
   */
  getSensorLineChartOneHourList(body: GetSensorInput): Observable<SensorReportLineChartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Dashboard/GetSensorLineChartOneHourList";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetSensorLineChartOneHourList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetSensorLineChartOneHourList(<any>response_);
            } catch (e) {
              return <Observable<SensorReportLineChartOutPut[]>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<SensorReportLineChartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGetSensorLineChartOneHourList(
    response: HttpResponseBase
  ): Observable<SensorReportLineChartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          // result200 = SensorReportLineChartOutPut.fromJS(resultData200);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<SensorReportLineChartOutPut[]>(<any>null);
  }

   /**
   * @param body (required)
   * @return Success
   */
  getSensorLineChartOneDayList(body: GetSensorInput): Observable<SensorReportLineChartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Dashboard/GetSensorLineChartOneDayList";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetSensorLineChartOneDayList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetSensorLineChartOneDayList(<any>response_);
            } catch (e) {
              return <Observable<SensorReportLineChartOutPut[]>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<SensorReportLineChartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGetSensorLineChartOneDayList(
    response: HttpResponseBase
  ): Observable<SensorReportLineChartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          // result200 = SensorReportLineChartOutPut.fromJS(resultData200);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<SensorReportLineChartOutPut[]>(<any>null);
  }

    /**
   * @param body (required)
   * @return Success
   */
  getSensorLineChartOneWeekList(body: GetSensorInput): Observable<SensorReportLineChartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Dashboard/GetSensorLineChartOneWeekList";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetSensorLineChartOneWeekList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetSensorLineChartOneWeekList(<any>response_);
            } catch (e) {
              return <Observable<SensorReportLineChartOutPut[]>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<SensorReportLineChartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGetSensorLineChartOneWeekList(
    response: HttpResponseBase
  ): Observable<SensorReportLineChartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          // result200 = SensorReportLineChartOutPut.fromJS(resultData200);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<SensorReportLineChartOutPut[]>(<any>null);
  }

    /**
   * @param body (required)
   * @return Success
   */
  getSensorLineChartOneMountList(body: GetSensorInput): Observable<SensorReportLineChartOutPut[]> {
    let url_ = this.baseUrl + "/api/services/app/Dashboard/GetSensorLineChartOneMountList";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(body);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json-patch+json",
        Accept: "text/plain",
      }),
    };

    return this.http
      .request("post", url_, options_)
      .pipe(
        _observableMergeMap((response_: any) => {
          return this.processGetSensorLineChartOneMountList(response_);
        })
      )
      .pipe(
        _observableCatch((response_: any) => {
          if (response_ instanceof HttpResponseBase) {
            try {
              return this.processGetSensorLineChartOneMountList(<any>response_);
            } catch (e) {
              return <Observable<SensorReportLineChartOutPut[]>>(<any>_observableThrow(e));
            }
          } else
            return <Observable<SensorReportLineChartOutPut[]>>(
              (<any>_observableThrow(response_))
            );
        })
      );
  }

  protected processGetSensorLineChartOneMountList(
    response: HttpResponseBase
  ): Observable<SensorReportLineChartOutPut[]> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse
        ? response.body
        : (<any>response).error instanceof Blob
        ? (<any>response).error
        : undefined;

    let _headers: any = {};
    if (response.headers) {
      for (let key of response.headers.keys()) {
        _headers[key] = response.headers.get(key);
      }
    }
    if (status === 200) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          // let result200: any = null;
          let resultData200 =
            _responseText === ""
              ? null
              : JSON.parse(_responseText, this.jsonParseReviver);
          // result200 = SensorReportLineChartOutPut.fromJS(resultData200);
          return _observableOf(resultData200);
        })
      );
    } else if (status !== 200 && status !== 204) {
      return this.utilsService.blobToText(responseBlob).pipe(
        _observableMergeMap((_responseText) => {
          return this.utilsService.throwException(
            "An unexpected server error occurred.",
            status,
            _responseText,
            _headers
          );
        })
      );
    }
    return _observableOf<SensorReportLineChartOutPut[]>(<any>null);
  }
}
