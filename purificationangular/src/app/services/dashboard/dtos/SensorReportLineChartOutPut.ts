import { isObject } from "util";

export class SensorReportLineChartOutPut
  implements ISensorReportLineChartOutPut {
  id: number;
  date: string;
  value: number;

  constructor(data?: ISensorReportLineChartOutPut) {
    if (data) {

      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (isObject(data)) {

        this.id = data.id;
        this.date = data.date;
        this.value = data.value;
      }

    }
  }

  static fromJS(data: any): SensorReportLineChartOutPut {
    
    data = typeof data === "object" ? data : {};

    let result = new SensorReportLineChartOutPut();

    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === "object" ? data : {};
    data["date"] = this.date;
    data["value"] = this.value;
    return data;
  }

  clone(): SensorReportLineChartOutPut {
    const json = this.toJSON();
    let result = new SensorReportLineChartOutPut();
    result.init(json);
    return result;
  }
}

export interface ISensorReportLineChartOutPut {
  id: number;
  date: string;
  value: number;
}
