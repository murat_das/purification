import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { TestResultFullOutPut } from "@app/services/testResult/dtos/TestResultFullOutPut";
import { TestResultService } from "@app/services/testResult/testResult.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteTestResultInput } from "@app/services/testResult/dtos/DeleteTestResultInput";

@Component({
  selector: "app-testResult",
  templateUrl: "./testResult.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./testResult.component.css"],
  providers: [],
})
export class TestResultComponent extends AppComponentBase implements OnInit {
  testResults: TestResultFullOutPut[] = [];
  dataSource: any = {};
  tenantHidden:boolean=true;

  constructor(
    injector: Injector,
    private _testResultService: TestResultService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
    
    this.tenantHidden=this.appSession.tenantId==null?false:true;

    console.log(this.appSession.tenantId,"this.appSession.tenantId");

  }

  createTestResult(): void {
    this._modalManagerService
      .openCreateTestResultDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editTestResult(id: number): void {
    this._modalManagerService
      .openEditTestResultDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteTestResult(testResult: DeleteTestResultInput): void {
    abp.message.confirm(
      this.l(
        "TestResultDeleteWarningMessage", testResult.id
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._testResultService
            .delete(testResult)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
    console.log(this.dataSource,"this.dataSource");

  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._testResultService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: TestResultFullOutPut[]) => {
                this.testResults = result;
                resolve(this.testResults);
              });
          });
        },
      }),
    });
  }
}
