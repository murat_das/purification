import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { DeviceFullOutPut } from '@app/services/device/dtos/DeviceFullOutPut';
import { DeviceService } from '@app/services/device/device.service';
import { GetDeviceInput } from '@app/services/device/dtos/GetDeviceInput';

@Component({
  selector: 'app-edit-device-dialog',
  templateUrl: './edit-device-dialog.component.html',
  styleUrls: ['./edit-device-dialog.component.css']
})
export class EditDeviceDialogComponent extends AppComponentBase implements OnInit {

  saving = false;
  device: DeviceFullOutPut = new DeviceFullOutPut();

  constructor(injector: Injector,
    public _deviceService: DeviceService,
    private _dialogRef: MatDialogRef<EditDeviceDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _device: GetDeviceInput) {
    super(injector);
  }

  ngOnInit() {
    this._deviceService.get(this._device).subscribe((result: DeviceFullOutPut) => {
      this.device = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._deviceService
      .update({name:this.device.name, id: this.device.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
