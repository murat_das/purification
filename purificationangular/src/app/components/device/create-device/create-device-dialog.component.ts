import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateDeviceInput } from "@app/services/device/dtos/CreateDeviceInput";
import { DeviceService } from "@app/services/device/device.service";
import { MatDialogRef } from "@angular/material";
import { finalize, startWith, map } from "rxjs/operators";
import { TenantService } from "@app/services/tenant/tenant.service";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: "app-create-device-dialog",
  templateUrl: "./create-device-dialog.component.html",
  styleUrls: ["./create-device-dialog.component.css"],
})
export class CreateDeviceDialogComponent extends AppComponentBase
  implements OnInit {
  // searchModeOption: string = "contains";
  // searchExprOption: any = "Name";
  // searchTimeoutOption: number = 200;
  // minSearchLengthOption: number = 0;

  myControl = new FormControl();
  filteredOptions: Observable<TenantPartOutPut[]>;

  saving = false;
  device: CreateDeviceInput = new CreateDeviceInput();
  tenants: TenantPartOutPut[] = [];

  constructor(
    injector: Injector,
    public _deviceService: DeviceService,
    public _tenantService: TenantService,
    private _dialogRef: MatDialogRef<CreateDeviceDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getTenants();
   
  }

  displayFn(tenant: TenantPartOutPut): string {
    // console.log(tenant,"tenanttenanttenant");

    return tenant && tenant.name ? tenant.name : "";
  }

  private _filter(name: string): TenantPartOutPut[] {
    const filterValue = name.toLowerCase();
    return this.tenants.filter(
      (tenant) => tenant.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  getTenants(): TenantPartOutPut[] | any {
    this._tenantService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: TenantPartOutPut[]) => {
        console.log(result, "result");
        this.tenants = result;
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.tenants.slice()))
        );
        return (this.tenants);
      });
  }

  save(): void {
    if (typeof this.device.tenant === "object") {
      this.saving = true;

      this._deviceService
        .create(this.device)
        .pipe(
          finalize(() => {
            this.saving = false;
          })
        )
        .subscribe(() => {
          this.notify.info(this.l("SavedSuccessfully"));
          this.close(true);
        });
    } else {
      this.notify.error(this.l("TenantSavedFailed"));
    }
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
