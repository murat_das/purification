import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { DeviceService } from "@app/services/device/device.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteDeviceInput } from "@app/services/device/dtos/DeleteDeviceInput";
import { TenantServiceProxy } from "@shared/service-proxies/service-proxies";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";
import { TenantService } from "@app/services/tenant/tenant.service";

@Component({
  selector: "app-device",
  templateUrl: "./device.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./device.component.css"],
  providers: [],
})
export class DeviceComponent extends AppComponentBase implements OnInit {
  devices: DeviceFullOutPut[] = [];
  dataSource: any = {};
  tenantHidden: boolean = true;
  tenants: TenantPartOutPut[] = [];

  constructor(
    injector: Injector,
    private _deviceService: DeviceService,
    public _tenantService: TenantService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
    // this.getTenants();

    this.tenantHidden = this.appSession.tenantId == null ? false : true;
  }

  createDevice(): void {
    this._modalManagerService
      .openCreateDeviceDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editDevice(id: number): void {
    this._modalManagerService
      .openEditDeviceDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteDevice(device: DeleteDeviceInput): void {
    abp.message.confirm(
      this.l("DeviceDeleteWarningMessage", device.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._deviceService
            .delete(device)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._deviceService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: DeviceFullOutPut[]) => {
                this.devices = result;
                console.log(this.devices, "this.devicesthis.devices");

                resolve(this.devices);
              });
          });
        },
      }),
    });
  }
}
