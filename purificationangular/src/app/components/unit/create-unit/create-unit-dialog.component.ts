import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateUnitInput } from '@app/services/unit/dtos/CreateUnitInput';
import { UnitService } from '@app/services/unit/unit.service';
import { MatDialogRef } from '@angular/material';
import { finalize, startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { TenantPartOutPut } from '@app/services/tenant/dtos/TenantPartOutPut';
import { TenantService } from '@app/services/tenant/tenant.service';

@Component({
  selector: 'app-create-unit-dialog',
  templateUrl: './create-unit-dialog.component.html',
  styleUrls: ['./create-unit-dialog.component.css']
})
export class CreateUnitDialogComponent extends AppComponentBase implements OnInit {

  saving=false;
  unit: CreateUnitInput = new CreateUnitInput();
  myControl = new FormControl();
  filteredOptions: Observable<TenantPartOutPut[]>;
  tenants: TenantPartOutPut[] = [];


  constructor(injector: Injector,
    public _tenantService: TenantService,
    public _unitService: UnitService,
    private _dialogRef: MatDialogRef<CreateUnitDialogComponent>) {
    super(injector);
  }

  ngOnInit() {
    this.getTenants();
   
  }

  displayFn(tenant: TenantPartOutPut): string {
    // console.log(tenant,"tenanttenanttenant");

    return tenant && tenant.name ? tenant.name : "";
  }

  private _filter(name: string): TenantPartOutPut[] {
    const filterValue = name.toLowerCase();
    return this.tenants.filter(
      (tenant) => tenant.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  getTenants(): TenantPartOutPut[] | any {
    this._tenantService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: TenantPartOutPut[]) => {
        console.log(result, "result");
        this.tenants = result
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.tenants.slice()))
        );
        return ( this.tenants);
      });
  }

  save(): void {
    this.saving = true;

    this._unitService
      .create(this.unit)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
