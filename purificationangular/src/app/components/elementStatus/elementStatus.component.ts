import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { ElementStatusFullOutPut } from "@app/services/elementStatus/dtos/ElementStatusFullOutPut";
import { ElementStatusService } from "@app/services/elementStatus/elementStatus.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteElementStatusInput } from "@app/services/elementStatus/dtos/DeleteElementStatusInput";

@Component({
  selector: "app-elementStatus",
  templateUrl: "./elementStatus.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./elementStatus.component.css"],
  providers: [],
})
export class ElementStatusComponent extends AppComponentBase implements OnInit {
  elementStatuses: ElementStatusFullOutPut[] = [];
  dataSource: any = {};
  tenantHidden:boolean=true;

  constructor(
    injector: Injector,
    private _elementStatusService: ElementStatusService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
    this.tenantHidden=this.appSession.tenantId==null?false:true;
  }

  createElementStatus(): void {
    this._modalManagerService
      .openCreateElementStatusDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editElementStatus(id: number): void {
    this._modalManagerService
      .openEditElementStatusDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteElementStatus(elementStatus: DeleteElementStatusInput): void {
    abp.message.confirm(
      this.l(
        "ElementStatusDeleteWarningMessage", elementStatus.id
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._elementStatusService
            .delete(elementStatus)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
    console.log(this.dataSource,"this.dataSource");

  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._elementStatusService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: ElementStatusFullOutPut[]) => {
                this.elementStatuses = result;
                resolve(this.elementStatuses);
              });
          });
        },
      }),
    });
  }
}
