import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateElementStatusInput } from "@app/services/elementStatus/dtos/CreateElementStatusInput";
import { ElementStatusService } from "@app/services/elementStatus/elementStatus.service";
import { MatDialogRef } from "@angular/material";
import { finalize, startWith, map } from "rxjs/operators";
import { EngineElementFullOutPut } from "@app/services/engineElement/dtos/EngineElementFullOutPut";
import { EngineElementService } from "@app/services/engineElement/engineElement.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: "app-create-elementStatus-dialog",
  templateUrl: "./create-elementStatus-dialog.component.html",
  styleUrls: ["./create-elementStatus-dialog.component.css"],
})
export class CreateElementStatusDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  elementStatus: CreateElementStatusInput = new CreateElementStatusInput();
  engineElements: EngineElementFullOutPut[] = [];

  myControl = new FormControl();
  filteredOptions: Observable<EngineElementFullOutPut[]>;

  constructor(
    injector: Injector,
    public _elementStatusService: ElementStatusService,
    public _engineElementService: EngineElementService,
    private _dialogRef: MatDialogRef<CreateElementStatusDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getEngineElements();

   
  }

  displayFn(engineElement: EngineElementFullOutPut): string {
    return engineElement && engineElement.name ? engineElement.name : "";
  }

  private _filter(name: string): EngineElementFullOutPut[] {
    const filterValue = name.toLowerCase();
    return this.engineElements.filter(
      (engineElement) => engineElement.name.toLowerCase().indexOf(filterValue) === 0
    );
  }


  getEngineElements(): EngineElementFullOutPut[] | any {
    this._engineElementService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: EngineElementFullOutPut[]) => {
        console.log(result, "resultresultresult");
        this.engineElements = result;
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.engineElements.slice()))
        );
        return ( this.engineElements);
      });
  }

  save(): void {
    this.saving = true;

    this._elementStatusService
      .create(this.elementStatus)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
