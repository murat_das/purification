import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateEngineElementInput } from "@app/services/engineElement/dtos/CreateEngineElementInput";
import { EngineElementService } from "@app/services/engineElement/engineElement.service";
import { MatDialogRef } from "@angular/material";
import { finalize, startWith, map } from "rxjs/operators";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { DeviceService } from "@app/services/device/device.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: "app-create-engineElement-dialog",
  templateUrl: "./create-engineElement-dialog.component.html",
  styleUrls: ["./create-engineElement-dialog.component.css"],
})
export class CreateEngineElementDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  engineElement: CreateEngineElementInput = new CreateEngineElementInput();
  devices: DeviceFullOutPut[] = [];

  myControl = new FormControl();
  filteredOptions: Observable<DeviceFullOutPut[]>;

  constructor(
    injector: Injector,
    public _engineElementService: EngineElementService,
    public _deviceService: DeviceService,
    private _dialogRef: MatDialogRef<CreateEngineElementDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getDevices();

   
  }

  displayFn(device: DeviceFullOutPut): string {
    return device && device.name ? device.name : "";
  }

  private _filter(name: string): DeviceFullOutPut[] {
    const filterValue = name.toLowerCase();
    return this.devices.filter(
      (device) => device.name.toLowerCase().indexOf(filterValue) === 0
    );
  }


  getDevices(): DeviceFullOutPut[] | any {
    this._deviceService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceFullOutPut[]) => {
        console.log(result, "resultresultresult");
        this.devices = result
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.devices.slice()))
        );
        return ( this.devices);
      });
  }

  save(): void {
    this.saving = true;

    this._engineElementService
      .create(this.engineElement)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
