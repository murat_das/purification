import { Component, OnInit, Injector, Optional, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { finalize, startWith, map } from "rxjs/operators";
import { AppComponentBase } from "@shared/app-component-base";
import { EngineElementFullOutPut } from "@app/services/engineElement/dtos/EngineElementFullOutPut";
import { EngineElementService } from "@app/services/engineElement/engineElement.service";
import { GetEngineElementInput } from "@app/services/engineElement/dtos/GetEngineElementInput";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { DeviceService } from "@app/services/device/device.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: "app-edit-engineElement-dialog",
  templateUrl: "./edit-engineElement-dialog.component.html",
  styleUrls: ["./edit-engineElement-dialog.component.css"],
})
export class EditEngineElementDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  engineElement: EngineElementFullOutPut = new EngineElementFullOutPut();
  devices: DeviceFullOutPut[] = [];
  
  myControl = new FormControl();
  filteredOptions: Observable<DeviceFullOutPut[]>;

  constructor(
    injector: Injector,
    public _engineElementService: EngineElementService,
    public _deviceService: DeviceService,
    private _dialogRef: MatDialogRef<EditEngineElementDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _engineElement: GetEngineElementInput
  ) {
    super(injector);
  }

  ngOnInit() {
    this._engineElementService
      .get(this._engineElement)
      .subscribe((result: EngineElementFullOutPut) => {
        this.engineElement = result;
        
      });

      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(""),
        map((value) => (typeof value === "string" ? value : value.name)),
        map((name) => (name ? this._filter(name) : this.devices.slice()))
      );

    this.getDevices();
  }

  displayFn(device: DeviceFullOutPut): string {
    return device && device.name ? device.name : "";
  }

  private _filter(name: string): DeviceFullOutPut[] {
    const filterValue = name.toLowerCase();
    return this.devices.filter(
      (device) => device.name.toLowerCase().indexOf(filterValue) === 0
    );
  }


 
  getDevices(): DeviceFullOutPut[] | any {
    this._deviceService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceFullOutPut[]) => {
        return (this.devices = result);
      });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._engineElementService
      .update({
        id: this.engineElement.id,
        name: this.engineElement.name,
        displayName: this.engineElement.displayName,
        status: this.engineElement.status,
        device: this.engineElement.device,
      })
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  customCompare(o1, o2) {
    return o1.id === o2.id;
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
