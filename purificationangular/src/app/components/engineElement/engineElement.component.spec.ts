/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EngineElementComponent } from './engineElement.component';

describe('EngineElementComponent', () => {
  let component: EngineElementComponent;
  let fixture: ComponentFixture<EngineElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
