import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { EngineElementFullOutPut } from "@app/services/engineElement/dtos/EngineElementFullOutPut";
import { EngineElementService } from "@app/services/engineElement/engineElement.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteEngineElementInput } from "@app/services/engineElement/dtos/DeleteEngineElementInput";

@Component({
  selector: "app-engineElement",
  templateUrl: "./engineElement.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./engineElement.component.css"],
  providers: [],
})
export class EngineElementComponent extends AppComponentBase implements OnInit {
  engineElements: EngineElementFullOutPut[] = [];
  dataSource: any = {};
  tenantHidden:boolean=true;

  constructor(
    injector: Injector,
    private _engineElementService: EngineElementService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
    this.tenantHidden=this.appSession.tenantId==null?false:true;
  }

  createEngineElement(): void {
    this._modalManagerService
      .openCreateEngineElementDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editEngineElement(id: number): void {
    this._modalManagerService
      .openEditEngineElementDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteEngineElement(engineElement: DeleteEngineElementInput): void {
    abp.message.confirm(
      this.l(
        "EngineElementDeleteWarningMessage", engineElement.id
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._engineElementService
            .delete(engineElement)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
    console.log(this.dataSource,"this.dataSource");

  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._engineElementService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: EngineElementFullOutPut[]) => {
                this.engineElements = result;
                resolve(this.engineElements);
              });
          });
        },
      }),
    });
  }
}
