import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { HttpClient } from "@angular/common/http";
import { appModuleAnimation } from "@shared/animations/routerTransition";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./profile.component.css"],
})
export class ProfileComponent extends AppComponentBase implements OnInit {
  uploadUrl: string;

  uploadedFiles: any[] = [];

  constructor(injector: Injector,private http: HttpClient, @Inject(HttpClient) httpClient: HttpClient) {
    super(injector);
    this.uploadUrl =
      "http://localhost:21021/api/services/app/tenant/uploadImage";
  }

  ngOnInit(): void {}

  myUploader(event): void {
    console.log("My File upload", event);

    if (event.files.length == 0) {
      console.log("No file selected.");

      return;
    }

    var fileToUpload = event.files[0];

    let input = new FormData();

    input.append("file", fileToUpload);
    console.log(fileToUpload);

    this.http
      .post(this.uploadUrl, fileToUpload)
      .subscribe((res) => {
      });
  }

  // upload completed event

  onUpload(event): void {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  onBeforeSend(event): void {
    event.xhr.setRequestHeader(
      "Authorization",
      "Bearer " + abp.auth.getToken()
    );
  }
}
