import { Component, OnInit, Injector, Inject, Pipe } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { MatDialog } from "@angular/material";
import { HttpClient } from "@angular/common/http";
import { AppComponentBase } from "@shared/app-component-base";
import DataSource from "devextreme/data/data_source";
import "devextreme/data/odata/store";
import { SensorReportLineChartOutPut } from "@app/services/dashboard/dtos/SensorReportLineChartOutPut";
import CustomStore from "devextreme/data/custom_store";
import { DashboardService } from "@app/services/dashboard/dashboard.service";
import { finalize, startWith, map } from "rxjs/operators";
import { GetSensorInput } from "@app/services/sensor/dtos/GetSensorInput";
import * as moment from "moment";

import { DatePipe } from "@angular/common";
import { SensorFullOutPut } from "@app/services/sensor/dtos/SensorFullOutPut";
import { SensorService } from "@app/services/sensor/sensor.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";

@Component({
  selector: "app-sensorDataReport",
  templateUrl: "./sensorDataReport.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./sensorDataReport.component.css"],
  providers: [DatePipe],
})
export class SensorDataReportComponent extends AppComponentBase
  implements OnInit {
  public sensorReportLineChartOutPut: SensorReportLineChartOutPut[] = new Array<
    SensorReportLineChartOutPut
  >();
  getSensor: GetSensorInput = new GetSensorInput();
  sensor: SensorFullOutPut = new SensorFullOutPut();

  sensors: SensorFullOutPut[] = [];
  chartDataSource: any;

  myControl = new FormControl();
  filteredOptions: Observable<SensorFullOutPut[]>;

  constructor(
    injector: Injector,
    private _dialog: MatDialog,
    public _sensorService: SensorService,
    public _dashboardService: DashboardService,

    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
    this.sensor.unit = new UnitPartOutPut();
    this.sensor.unit.name = "";
    this.getSensors();
    console.log(this.chartDataSource);
  }

  customizeTooltip(arg) {
    return {
      text: arg.valueText + "&#176C",
    };
  }
  //   customizeText(arg) {
  //     this.datePipe=new DatePipe(arg.value);
  //     let date=moment(arg.value, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss");
  //     // console.log(this.datePipe.transform(arg.value,"dd/MM/yyyy HH:mm:ss"));
  //     // console.log(date);

  //     return date;
  // }
  // onValueChanged(data) {
  //   this.chartDataSource.filter(["Id", "=", data.value]);
  //   this.chartDataSource.load();
  // }

  ngOnInit() {}


  displayFn(sensor: SensorFullOutPut): string {
    return sensor && sensor.displayName ? sensor.displayName : "";
  }

  selected(e) {
    this.chartDataSource = this.createDxDataSource();
  }

  getSensors(): SensorFullOutPut[] | any {
    this._sensorService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: SensorFullOutPut[]) => {
        this.sensors = result;
        console.log(this.sensors, " this.sensors this.sensors");
        this.sensor = this.sensors[0];
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.sensors.slice()))
        );
        this.chartDataSource = this.createDxDataSource();
        return this.sensors;
      });
  }

  private _filter(displayName: string): SensorFullOutPut[] {
    const filterValue = displayName.toLowerCase();
    let control = this.sensors.filter(
      (sensor) => sensor.displayName.toLowerCase().indexOf(filterValue) === 0
    );
    return control;
  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._dashboardService
              .getSensorLineChartList(this.sensor)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorReportLineChartOutPut[]) => {
                this.sensorReportLineChartOutPut = result;
                console.log(
                  this.sensorReportLineChartOutPut,
                  "this.sensorReportLineChartOutPut"
                );

                resolve(this.sensorReportLineChartOutPut);
              });
          });
        },
      }),
    });
  }

  oneHour() {
    this.chartDataSource = this.createDxDataSourceOneHour();
  }

  createDxDataSourceOneHour(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._dashboardService
              .getSensorLineChartOneHourList(this.sensor)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorReportLineChartOutPut[]) => {
                this.sensorReportLineChartOutPut = result;
                console.log(
                  this.sensorReportLineChartOutPut,
                  "this.sensorReportLineChartOutPut"
                );

                resolve(this.sensorReportLineChartOutPut);
              });
          });
        },
      }),
    });
  }
  
  oneDay() {
    this.chartDataSource = this.createDxDataSourceOneDay();
  }

  createDxDataSourceOneDay(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._dashboardService
              .getSensorLineChartOneDayList(this.sensor)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorReportLineChartOutPut[]) => {
                this.sensorReportLineChartOutPut = result;
                console.log(
                  this.sensorReportLineChartOutPut,
                  "this.sensorReportLineChartOutPut"
                );

                resolve(this.sensorReportLineChartOutPut);
              });
          });
        },
      }),
    });
  }

  oneWeek() {
    this.chartDataSource = this.createDxDataSourceOneWeek();
  }

  createDxDataSourceOneWeek(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._dashboardService
              .getSensorLineChartOneWeekList(this.sensor)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorReportLineChartOutPut[]) => {
                this.sensorReportLineChartOutPut = result;
                console.log(
                  this.sensorReportLineChartOutPut,
                  "this.sensorReportLineChartOutPut"
                );

                resolve(this.sensorReportLineChartOutPut);
              });
          });
        },
      }),
    });
  }

  oneMount() {
    this.chartDataSource = this.createDxDataSourceOneMount();
  }

  createDxDataSourceOneMount(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._dashboardService
              .getSensorLineChartOneMountList(this.sensor)
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorReportLineChartOutPut[]) => {
                this.sensorReportLineChartOutPut = result;
                console.log(
                  this.sensorReportLineChartOutPut,
                  "this.sensorReportLineChartOutPut"
                );

                resolve(this.sensorReportLineChartOutPut);
              });
          });
        },
      }),
    });
  }
}
