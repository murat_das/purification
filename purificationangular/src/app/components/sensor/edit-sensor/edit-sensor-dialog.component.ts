import { Component, OnInit, Injector, Optional, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/app-component-base";
import { SensorFullOutPut } from "@app/services/sensor/dtos/SensorFullOutPut";
import { SensorService } from "@app/services/sensor/sensor.service";
import { GetSensorInput } from "@app/services/sensor/dtos/GetSensorInput";
import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { DeviceService } from "@app/services/device/device.service";
import { UnitService } from "@app/services/unit/unit.service";
import { TenantPartOutPut } from "@app/services/tenant/dtos/TenantPartOutPut";

@Component({
  selector: "app-edit-sensor-dialog",
  templateUrl: "./edit-sensor-dialog.component.html",
  styleUrls: ["./edit-sensor-dialog.component.css"],
})
export class EditSensorDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  sensor: SensorFullOutPut = new SensorFullOutPut();
  units: UnitPartOutPut[] = [];
  devices: DeviceFullOutPut[] = [];

  constructor(
    injector: Injector,
    public _sensorService: SensorService,
    public _unitService: UnitService,
    public _deviceService: DeviceService,
    private _dialogRef: MatDialogRef<EditSensorDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _sensor: GetSensorInput
  ) {
    super(injector);
  }

  ngOnInit() {
    this._sensorService
      .get(this._sensor)
      .subscribe((result: SensorFullOutPut) => {
        this.sensor = result;
       this.getUnits(this.sensor.tenant);

      });

    // this.getDevices();
  }

  getUnits(tenant:TenantPartOutPut): UnitPartOutPut[] | any {
    this._unitService
      .getTenantUnitList(tenant)
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: UnitPartOutPut[]) => {
        return (this.units = result);
      });
  }

  getDevices(): DeviceFullOutPut[] | any {
    this._deviceService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceFullOutPut[]) => {
        return (this.devices = result);
      });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._sensorService
      .update({
        id: this.sensor.id,
        name: this.sensor.name,
        displayName: this.sensor.displayName,
        unit: this.sensor.unit,
        // device: this.sensor.device,
      })
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  customCompare(o1, o2) {
    return o1.id === o2.id;
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
