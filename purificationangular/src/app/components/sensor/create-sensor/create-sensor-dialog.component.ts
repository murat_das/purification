import { Component, OnInit, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { CreateSensorInput } from "@app/services/sensor/dtos/CreateSensorInput";
import { SensorService } from "@app/services/sensor/sensor.service";
import { MatDialogRef } from "@angular/material";
import { finalize, startWith, map } from "rxjs/operators";
import DataSource from "devextreme/data/data_source";
import CustomStore from "devextreme/data/custom_store";
import { UnitPartOutPut } from "@app/services/unit/dtos/UnitPartOutPut";
import { DeviceFullOutPut } from "@app/services/device/dtos/DeviceFullOutPut";
import { DeviceService } from "@app/services/device/device.service";
import { UnitService } from "@app/services/unit/unit.service";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { UnitFullOutPut } from "@app/services/unit/dtos/UnitFullOutPut";

@Component({
  selector: "app-create-sensor-dialog",
  templateUrl: "./create-sensor-dialog.component.html",
  styleUrls: ["./create-sensor-dialog.component.css"],
})
export class CreateSensorDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  sensor: CreateSensorInput = new CreateSensorInput();
  units: UnitPartOutPut[] = [];
  devices: DeviceFullOutPut[] = [];

  myControl = new FormControl();
  filteredOptions: Observable<DeviceFullOutPut[]>;

  constructor(
    injector: Injector,
    public _sensorService: SensorService,
    public _unitService: UnitService,
    public _deviceService: DeviceService,
    private _dialogRef: MatDialogRef<CreateSensorDialogComponent>
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getDevices();

    
  }

  displayFn(device: DeviceFullOutPut): string {
    // console.log(tenant,"tenanttenanttenant");
    if (device != null) {
      if (device != undefined) {
        console.log(device, "devicedevice");
        // this.getUnitSelectDeviceList(device);
      }
    }
    return device && device.name ? device.name : "";
  }

  private _filter(name: string): DeviceFullOutPut[] {
    const filterValue = name.toLowerCase();
    return this.devices.filter(
      (device) => device.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  getUnitSelectDeviceList(device: DeviceFullOutPut){
    console.log(device, "tenantId");

    this._unitService
      .getUnitSelectDeviceList(device)
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: UnitFullOutPut[]) => {
        return (this.units = result);
      });
  }

  getDevices(): DeviceFullOutPut[] | any {
    this._deviceService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceFullOutPut[]) => {
        console.log(result, "resultresultresult");
        this.devices = result
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(""),
          map((value) => (typeof value === "string" ? value : value.name)),
          map((name) => (name ? this._filter(name) : this.devices.slice()))
        );
        return ( this.devices);
      });
  }

  save(): void {
    this.saving = true;

    this._sensorService
      .create(this.sensor)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
