import { Component, OnInit, Injector, Inject } from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { SensorFullOutPut } from "@app/services/sensor/dtos/SensorFullOutPut";
import { SensorService } from "@app/services/sensor/sensor.service";
import { ModalManagerService } from "@app/services/common/modal-manager.service";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import DataSource from "devextreme/data/data_source";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import CustomStore from "devextreme/data/custom_store";
import { finalize } from "rxjs/operators";
import { DeleteSensorInput } from "@app/services/sensor/dtos/DeleteSensorInput";

@Component({
  selector: "app-sensor",
  templateUrl: "./sensor.component.html",
  animations: [appModuleAnimation()],
  styleUrls: ["./sensor.component.css"],
  providers: [],
})
export class SensorComponent extends AppComponentBase implements OnInit {
  sensors: SensorFullOutPut[] = [];
  dataSource: any = {};
  tenantHidden:boolean=true;

  constructor(
    injector: Injector,
    private _sensorService: SensorService,
    private _modalManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
    this.tenantHidden=this.appSession.tenantId==null?false:true;
  }

  createSensor(): void {
    this._modalManagerService
      .openCreateSensorDialog()
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  editSensor(id: number): void {
    this._modalManagerService
      .openEditSensorDialog(id)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.refreshDataGrid();
        }
      });
  }

  deleteSensor(sensor: DeleteSensorInput): void {
    abp.message.confirm(
      this.l(
        "SensorDeleteWarningMessage", sensor.id
      ),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._sensorService
            .delete(sensor)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l("SuccessfullyDeleted"));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items
      .unshift
      // {
      //   location: 'before',
      //   template: 'formNameTemplate'
      // },
      // {
      //   location: 'after',
      //   template: 'refreshButtonTemplate'
      // }
      ();
  }

  // dxGrid Changed
  onOptionChanged(e) {
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
    console.log(this.dataSource,"this.dataSource");

  }

  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._sensorService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: SensorFullOutPut[]) => {
                this.sensors = result;
                resolve(this.sensors);
              });
          });
        },
      }),
    });
  }
}
