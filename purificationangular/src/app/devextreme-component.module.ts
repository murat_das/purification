import { NgModule } from '@angular/core';
import { DxButtonModule, DxDataGridModule, DxTooltipModule, DxSelectBoxModule, DxDropDownBoxModule, DxTextBoxModule, DxFileUploaderModule, DxChartModule } from 'devextreme-angular';

@NgModule({
    imports: [
        DxButtonModule,
        DxDataGridModule,
        DxTooltipModule
    ],
    exports: [
      DxButtonModule,
      DxDataGridModule,
      DxTooltipModule,
      DxDropDownBoxModule,
      DxTextBoxModule,
      DxSelectBoxModule,
      DxFileUploaderModule,
      DxChartModule,
    ]
})
export class DevextremeComponentModule { }
