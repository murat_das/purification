import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
    templateUrl: './sidebar-nav.component.html',
    selector: 'sidebar-nav',
    encapsulation: ViewEncapsulation.None
})
export class SideBarNavComponent extends AppComponentBase {

    menuItems: MenuItem[] = [
        // new MenuItem(this.l('HomePage'), '', 'home', '/app/home'),
        new MenuItem(this.l('Reports'), '', 'show_chart', '', [
            new MenuItem(this.l('SensorDataReport'), '', 'bubble_chart', '/app/sensorDataReports',),
        ]),
        new MenuItem(this.l('Definitions'), '', 'business', '', [
            new MenuItem(this.l('Device'), 'Device.GetList', 'inbox', '/app/devices',),
            new MenuItem(this.l('Unit'), 'Unit.GetList', 'inbox', '/app/units',),
            new MenuItem(this.l('Sensor'), 'Sensor.GetList', 'inbox', '/app/sensors',),
            new MenuItem(this.l('TestResult'), 'TestResult.GetList', 'inbox', '/app/testResults',),
            new MenuItem(this.l('EngineElement'), 'EngineElement.GetList', 'inbox', '/app/engineElements',),
            new MenuItem(this.l('ElementStatus'), 'ElementStatus.GetList', 'inbox', '/app/elementStatuses',),
        ]),


        
        new MenuItem(this.l('Tenants'), 'Pages.Tenants', 'business', '/app/tenants'),
        new MenuItem(this.l('Users'), 'Pages.Users.Menu', 'people', '/app/users'),
        new MenuItem(this.l('Roles'), 'Pages.Roles', 'local_offer', '/app/roles'),
        new MenuItem(this.l('About'), '', 'info', '/app/about'),
    ];

    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    showMenuItem(menuItem): boolean {
        if (menuItem.permissionName) {
            return this.permission.isGranted(menuItem.permissionName);
        }

        return true;
    }
}
