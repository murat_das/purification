import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientJsonpModule } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";

import { ModalModule } from "ngx-bootstrap/modal";
import { NgxPaginationModule } from "ngx-pagination";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { AbpModule } from "@abp/abp.module";

import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { SharedModule } from "@shared/shared.module";

import { HomeComponent } from "@app/home/home.component";
import { AboutComponent } from "@app/about/about.component";
import { TopBarComponent } from "@app/layout/topbar.component";
import { TopBarLanguageSwitchComponent } from "@app/layout/topbar-languageswitch.component";
import { SideBarUserAreaComponent } from "@app/layout/sidebar-user-area.component";
import { SideBarNavComponent } from "@app/layout/sidebar-nav.component";
import { SideBarFooterComponent } from "@app/layout/sidebar-footer.component";
import { RightSideBarComponent } from "@app/layout/right-sidebar.component";
// tenants
import { TenantsComponent } from "@app/tenants/tenants.component";
import { CreateTenantDialogComponent } from "./tenants/create-tenant/create-tenant-dialog.component";
import { EditTenantDialogComponent } from "./tenants/edit-tenant/edit-tenant-dialog.component";
// roles
import { RolesComponent } from "@app/roles/roles.component";
import { CreateRoleDialogComponent } from "./roles/create-role/create-role-dialog.component";
import { EditRoleDialogComponent } from "./roles/edit-role/edit-role-dialog.component";
// users
import { UsersComponent } from "@app/users/users.component";
import { CreateUserDialogComponent } from "@app/users/create-user/create-user-dialog.component";
import { EditUserDialogComponent } from "@app/users/edit-user/edit-user-dialog.component";
import { ChangePasswordComponent } from "./users/change-password/change-password.component";
import { ResetPasswordDialogComponent } from "./users/reset-password/reset-password.component";
import { DevextremeComponentModule } from "./devextreme-component.module";
import { PurificationServiceModule } from "./services/purification-service.module";
import { DeviceComponent } from "./components/device/device.component";
import { CreateDeviceDialogComponent } from "./components/device/create-device/create-device-dialog.component";
import { EditDeviceDialogComponent } from "./components/device/edit-device/edit-device-dialog.component";
import { CreateUnitDialogComponent } from "./components/unit/create-unit/create-unit-dialog.component";
import { EditUnitDialogComponent } from "./components/unit/edit-unit/edit-unit-dialog.component";
import { UnitComponent } from "./components/unit/unit.component";
import { SensorComponent } from "./components/sensor/sensor.component";
import { CreateSensorDialogComponent } from "./components/sensor/create-sensor/create-sensor-dialog.component";
import { EditSensorDialogComponent } from "./components/sensor/edit-sensor/edit-sensor-dialog.component";
import { TestResultComponent } from "./components/testResult/testResult.component";
import { CreateTestResultDialogComponent } from "./components/testResult/create-testResult/create-testResult-dialog.component";
import { EditTestResultDialogComponent } from "./components/testResult/edit-testResult/edit-testResult-dialog.component";
import { EngineElementComponent } from "./components/engineElement/engineElement.component";
import { CreateEngineElementDialogComponent } from "./components/engineElement/create-engineElement/create-engineElement-dialog.component";
import { EditEngineElementDialogComponent } from "./components/engineElement/edit-engineElement/edit-engineElement-dialog.component";
import { ElementStatusComponent } from "./components/elementStatus/elementStatus.component";
import { CreateElementStatusDialogComponent } from "./components/elementStatus/create-elementStatus/create-elementStatus-dialog.component";
import { EditElementStatusDialogComponent } from "./components/elementStatus/edit-elementStatus/edit-elementStatus-dialog.component";
import { ProfileComponent } from "./components/profile/profile.component";
import {FileUploadModule} from 'primeng/fileupload';
import { SensorDataReportComponent } from "./components/dashboard/sensorDataReport/sensorDataReport.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    TopBarLanguageSwitchComponent,
    SideBarUserAreaComponent,
    SideBarNavComponent,
    SideBarFooterComponent,
    RightSideBarComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // devices
    DeviceComponent,
    CreateDeviceDialogComponent,
    EditDeviceDialogComponent,
    // units
    UnitComponent,
    CreateUnitDialogComponent,
    EditUnitDialogComponent,
    // sensors
    SensorComponent,
    CreateSensorDialogComponent,
    EditSensorDialogComponent,
    // testResults
    TestResultComponent,
    CreateTestResultDialogComponent,
    EditTestResultDialogComponent,
    // engineElements
    EngineElementComponent,
    CreateEngineElementDialogComponent,
    EditEngineElementDialogComponent,
    // elementStatuses
    ElementStatusComponent,
    CreateElementStatusDialogComponent,
    EditElementStatusDialogComponent,
    //profiles
    ProfileComponent,
    //reports
    SensorDataReportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    PurificationServiceModule,
    DevextremeComponentModule,
    FileUploadModule,
  ],
  providers: [DatePipe],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // devices
    CreateDeviceDialogComponent,
    EditDeviceDialogComponent,
    // units
    CreateUnitDialogComponent,
    EditUnitDialogComponent,
    // sensors
    SensorComponent,
    CreateSensorDialogComponent,
    EditSensorDialogComponent,
    // testResults
    CreateTestResultDialogComponent,
    EditTestResultDialogComponent,
    // engineElements
    CreateEngineElementDialogComponent,
    EditEngineElementDialogComponent,
    // elementStatuses
    CreateElementStatusDialogComponent,
    EditElementStatusDialogComponent,
  ],
})
export class AppModule {}
