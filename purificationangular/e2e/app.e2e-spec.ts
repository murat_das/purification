import { PurificationTemplatePage } from './app.po';

describe('Purification App', function() {
  let page: PurificationTemplatePage;

  beforeEach(() => {
    page = new PurificationTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
