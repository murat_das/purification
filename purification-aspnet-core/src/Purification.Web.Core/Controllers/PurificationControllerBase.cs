using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Purification.Controllers
{
    public abstract class PurificationControllerBase: AbpController
    {
        protected PurificationControllerBase()
        {
            LocalizationSourceName = PurificationConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
