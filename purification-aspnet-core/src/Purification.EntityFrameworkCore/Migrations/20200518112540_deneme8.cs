﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Purification.Migrations
{
    public partial class deneme8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_TestResult_TenantId",
                table: "TestResult",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_TestResult_AbpTenants_TenantId",
                table: "TestResult",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TestResult_AbpTenants_TenantId",
                table: "TestResult");

            migrationBuilder.DropIndex(
                name: "IX_TestResult_TenantId",
                table: "TestResult");
        }
    }
}
