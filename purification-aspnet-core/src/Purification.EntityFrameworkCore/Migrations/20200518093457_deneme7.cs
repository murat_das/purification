﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Purification.Migrations
{
    public partial class deneme7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Sensor_TenantId",
                table: "Sensor",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sensor_AbpTenants_TenantId",
                table: "Sensor",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sensor_AbpTenants_TenantId",
                table: "Sensor");

            migrationBuilder.DropIndex(
                name: "IX_Sensor_TenantId",
                table: "Sensor");
        }
    }
}
