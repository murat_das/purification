﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Purification.Migrations
{
    public partial class deneme6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Unit_TenantId",
                table: "Unit",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Unit_AbpTenants_TenantId",
                table: "Unit",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Unit_AbpTenants_TenantId",
                table: "Unit");

            migrationBuilder.DropIndex(
                name: "IX_Unit_TenantId",
                table: "Unit");
        }
    }
}
