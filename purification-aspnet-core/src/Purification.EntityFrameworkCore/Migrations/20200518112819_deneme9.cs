﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Purification.Migrations
{
    public partial class deneme9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_EngineElement_TenantId",
                table: "EngineElement",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_ElementStatus_TenantId",
                table: "ElementStatus",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_ElementStatus_AbpTenants_TenantId",
                table: "ElementStatus",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EngineElement_AbpTenants_TenantId",
                table: "EngineElement",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ElementStatus_AbpTenants_TenantId",
                table: "ElementStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_EngineElement_AbpTenants_TenantId",
                table: "EngineElement");

            migrationBuilder.DropIndex(
                name: "IX_EngineElement_TenantId",
                table: "EngineElement");

            migrationBuilder.DropIndex(
                name: "IX_ElementStatus_TenantId",
                table: "ElementStatus");
        }
    }
}
