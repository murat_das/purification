﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Purification.Migrations
{
    public partial class deneme5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Device_TenantId",
                table: "Device",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Device_AbpTenants_TenantId",
                table: "Device",
                column: "TenantId",
                principalTable: "AbpTenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Device_AbpTenants_TenantId",
                table: "Device");

            migrationBuilder.DropIndex(
                name: "IX_Device_TenantId",
                table: "Device");
        }
    }
}
