﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Purification.Configuration;
using Purification.Web;

namespace Purification.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class PurificationDbContextFactory : IDesignTimeDbContextFactory<PurificationDbContext>
    {
        public PurificationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PurificationDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            builder.UseLazyLoadingProxies();

            PurificationDbContextConfigurer.Configure(builder, configuration.GetConnectionString(PurificationConsts.ConnectionStringName));

            return new PurificationDbContext(builder.Options);
        }
    }
}
