using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Purification.EntityFrameworkCore
{
    public static class PurificationDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PurificationDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PurificationDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
