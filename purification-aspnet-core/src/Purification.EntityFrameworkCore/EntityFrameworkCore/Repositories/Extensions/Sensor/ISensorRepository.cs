﻿using Abp.Domain.Repositories;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{ 
    public interface ISensorRepository : IRepository<Sensor, int>
    {

    }
}