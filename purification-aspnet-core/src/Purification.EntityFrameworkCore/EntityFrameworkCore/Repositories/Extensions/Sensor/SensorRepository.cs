﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class SensorRepository : PurificationRepositoryBase<Sensor, int>, ISensorRepository
    {
        public SensorRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}