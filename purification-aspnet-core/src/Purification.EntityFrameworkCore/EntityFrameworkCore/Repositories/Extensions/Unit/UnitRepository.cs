﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class UnitRepository : PurificationRepositoryBase<Unit, int>, IUnitRepository
    {
        public UnitRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}