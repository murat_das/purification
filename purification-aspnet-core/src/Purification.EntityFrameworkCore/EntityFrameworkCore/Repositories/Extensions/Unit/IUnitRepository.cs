﻿using Abp.Domain.Repositories;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{ 
    public interface IUnitRepository : IRepository<Unit, int>
    {

    }
}