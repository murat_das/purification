﻿using Abp.Domain.Repositories;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public interface IDeviceRepository : IRepository<Device, int>
    {

    }
}