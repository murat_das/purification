﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class DeviceRepository : PurificationRepositoryBase<Device, int>, IDeviceRepository
    {
        public DeviceRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}