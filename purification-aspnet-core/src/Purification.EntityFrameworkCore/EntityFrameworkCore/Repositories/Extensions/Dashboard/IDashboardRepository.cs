﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public interface IDashboardRepository : IRepository<Sensor, int>
    {
    }
}