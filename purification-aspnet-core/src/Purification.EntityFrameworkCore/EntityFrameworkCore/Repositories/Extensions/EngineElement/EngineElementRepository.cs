﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class EngineElementRepository : PurificationRepositoryBase<EngineElement, int>, IEngineElementRepository
    {
        public EngineElementRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}