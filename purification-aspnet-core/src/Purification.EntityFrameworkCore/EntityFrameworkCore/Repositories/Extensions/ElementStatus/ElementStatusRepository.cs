﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class ElementStatusRepository : PurificationRepositoryBase<ElementStatus, int>, IElementStatusRepository
    {
        public ElementStatusRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}