﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Abp.Data;
using Abp.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories.Extensions;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class TestResultRepository : PurificationRepositoryBase<TestResult, int>, ITestResultRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;
        public TestResultRepository(IDbContextProvider<PurificationDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public async Task<List<TestResultOneHourData>> GetOneDayTestResult(int sensorId)
        {

            using (var command = CreateCommand("AvgSensorResultDataForHour", CommandType.StoredProcedure, new SqlParameter("@SensorId", sensorId)))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<TestResultOneHourData>();

                    while (dataReader.Read())
                    {
                        TestResultOneHourData data = new TestResultOneHourData
                        {
                            Date = dataReader["Date"].ToString(),
                            Value = Convert.ToDecimal(dataReader["Avg"])
                        };
                        result.Add(data);
                    }

                    return result;
                }
            }
        }

        public async Task<List<TestResultOneHourData>> GetOneWeekTestResult(int sensorId)
        {
            using (var command = CreateCommand("AvgSensorResultDataForDay", CommandType.StoredProcedure, new SqlParameter("@SensorId", sensorId)))
            {
                using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<TestResultOneHourData>();

                    while (dataReader.Read())
                    {
                        TestResultOneHourData data = new TestResultOneHourData
                        {
                            Date = dataReader["Date"].ToString(),
                            Value = Convert.ToDecimal(dataReader["Avg"])
                        };
                        result.Add(data);
                    }

                    return result;
                }
            }
        }

        public async Task<List<TestResultOneHourData>> GetOneMountTestResult(int sensorId)
        {

            using (var command = CreateCommand("AvgSensorResultDataForWeek", CommandType.StoredProcedure, new SqlParameter("@SensorId", sensorId)))
            {
                await using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<TestResultOneHourData>();

                    while (dataReader.Read())
                    {
                        TestResultOneHourData data = new TestResultOneHourData
                        {
                            Date = dataReader["Date"].ToString(),
                            Value = Convert.ToDecimal(dataReader["Avg"])
                        };
                        result.Add(data);
                    }

                    return result;
                }
            }
        }


        public async Task<List<TestResultOneHourData>> GetOneYearTestResult(int sensorId)
        {

            using (var command = CreateCommand("AvgSensorResultDataForMount", CommandType.StoredProcedure, new SqlParameter("@SensorId", sensorId)))
            {
                await using (var dataReader = await command.ExecuteReaderAsync())
                {
                    var result = new List<TestResultOneHourData>();

                    while (dataReader.Read())
                    {
                        TestResultOneHourData data = new TestResultOneHourData
                        {
                            Date = dataReader["Date"].ToString(),
                            Value = Convert.ToDecimal(dataReader["Avg"])
                        };
                        result.Add(data);
                    }

                    return result;
                }
            }
        }


        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = Context.Database.GetDbConnection().CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

        private async Task EnsureConnectionOpenAsync()
        {
            var connection = Context.Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
            {
                await connection.OpenAsync();
            }
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(PurificationDbContext) },
                {"MultiTenancySide", MultiTenancySide }
            });
        }
    }
}