﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories.Extensions;

namespace Purification.EntityFrameworkCore.Repositories
{
    public interface ITestResultRepository : IRepository<TestResult, int>
    {
        Task<List<TestResultOneHourData>> GetOneDayTestResult(int sensorId);
        Task<List<TestResultOneHourData>> GetOneWeekTestResult(int sensorId);
        Task<List<TestResultOneHourData>> GetOneMountTestResult(int sensorId);
        Task<List<TestResultOneHourData>> GetOneYearTestResult(int sensorId);

    }
}