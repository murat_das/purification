﻿using System;
using Abp.Application.Services.Dto;

namespace Purification.EntityFrameworkCore.Repositories.Extensions
{
    public class TestResultOneHourData : EntityDto<int>
    {

        public string Date { get; set; }
      
        public decimal Value { get; set; }
    }
}