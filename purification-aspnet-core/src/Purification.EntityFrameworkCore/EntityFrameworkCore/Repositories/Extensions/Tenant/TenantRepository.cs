﻿using Abp.EntityFrameworkCore;
using Purification.Domain.Entities;
using Purification.MultiTenancy;

namespace Purification.EntityFrameworkCore.Repositories
{
    public class TenantRepository : PurificationRepositoryBase<Tenant, int>, ITenantRepository
    {
        public TenantRepository(IDbContextProvider<PurificationDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}