﻿using Abp.Domain.Repositories;
using Purification.MultiTenancy;

namespace Purification.EntityFrameworkCore.Repositories
{
    public interface ITenantRepository : IRepository<Tenant, int>
    {
        
    }
}