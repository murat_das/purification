﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Purification.Authorization.Roles;
using Purification.Authorization.Users;
using Purification.Domain.Configurations;
using Purification.Domain.Entities;
using Purification.MultiTenancy;

namespace Purification.EntityFrameworkCore
{
    public class PurificationDbContext : AbpZeroDbContext<Tenant, Role, User, PurificationDbContext>
    {
        /* Define a DbSet for each entity of the application */
        #region DBSet
        public DbSet<Device> Devices { get; set; }
        public DbSet<ElementStatus> ElementStatuses { get; set; }
        public DbSet<EngineElement> EngineElements { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
        #endregion DBSet

        public PurificationDbContext(DbContextOptions<PurificationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfiguration(new DeviceConfiguration())
                .ApplyConfiguration(new ElementStatusConfiguration())
                .ApplyConfiguration(new EngineElementConfiguration())
                .ApplyConfiguration(new SensorConfiguration())
                .ApplyConfiguration(new TestResultConfiguration())
                .ApplyConfiguration(new UnitConfiguration());
        }
    }
}
