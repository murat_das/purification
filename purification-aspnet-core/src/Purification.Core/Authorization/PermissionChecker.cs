﻿using Abp.Authorization;
using Purification.Authorization.Roles;
using Purification.Authorization.Users;

namespace Purification.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
