﻿namespace Purification.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";
        public const string Pages_Users_Menu = "Pages.Users.Menu";

        public const string Pages_Roles = "Pages.Roles";

        #region Devices
        public const string Device = "Device";
        //public const string Device_Create = "Device.Create";
        public const string Device_Get = "Device.Get";
        public const string Device_GetList = "Device.GetList";
        //public const string Device_Delete = "Device.Delete";
        //public const string Device_Update = "Device.Update";
        #endregion

        #region ElementStatuses
        public const string ElementStatus = "ElementStatus";
        //public const string ElementStatus_Create = "ElementStatus.Create";
        public const string ElementStatus_Get = "ElementStatus.Get";
        public const string ElementStatus_GetList = "ElementStatus.GetList";
        //public const string ElementStatus_Delete = "ElementStatus.Delete";
        //public const string ElementStatus_Update = "ElementStatus.Update";
        #endregion

        #region EngineElements
        public const string EngineElement = "EngineElement";
        //public const string EngineElement_Create = "EngineElement.Create";
        public const string EngineElement_Get = "EngineElement.Get";
        public const string EngineElement_GetList = "EngineElement.GetList";
        //public const string EngineElement_Delete = "EngineElement.Delete";
        //public const string EngineElement_Update = "EngineElement.Update";
        #endregion

        #region Sensors
        public const string Sensor = "Sensor";
        //public const string Sensor_Create = "Sensor.Create";
        public const string Sensor_Get = "Sensor.Get";
        public const string Sensor_GetList = "Sensor.GetList";
        //public const string Sensor_Delete = "Sensor.Delete";
        //public const string Sensor_Update = "Sensor.Update";
        #endregion

        #region TestResults
        public const string TestResult = "TestResult";
        //public const string TestResult_Create = "TestResult.Create";
        public const string TestResult_Get = "TestResult.Get";
        public const string TestResult_GetList = "TestResult.GetList";
        //public const string TestResult_Delete = "TestResult.Delete";
        //public const string TestResult_Update = "TestResult.Update";
        #endregion

        #region Units
        public const string Unit = "Unit";
        //public const string Unit_Create = "Unit.Create";
        public const string Unit_Get = "Unit.Get";
        public const string Unit_GetList = "Unit.GetList";
        //public const string Unit_Delete = "Unit.Delete";
        //public const string Unit_Update = "Unit.Update";
        #endregion
    }
}
