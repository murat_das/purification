﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Purification.Authorization
{
    public class PurificationAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Users_Menu, L("Permission_Users_Menu"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            // Devices
            context.CreatePermission(PermissionNames.Device, L("Permission_Device"));
            //context.CreatePermission(PermissionNames.Device_Create, L("Permission_Device_Create"));
            context.CreatePermission(PermissionNames.Device_Get, L("Permission_Device_Get"));
            context.CreatePermission(PermissionNames.Device_GetList, L("Permission_Device_GetList"));
            //context.CreatePermission(PermissionNames.Device_Delete, L("Permission_Device_Delete"));
            //context.CreatePermission(PermissionNames.Device_Update, L("Permission_Device_Update"));

            // ElementStatuses
            context.CreatePermission(PermissionNames.ElementStatus, L("Permission_ElementStatus"));
            //context.CreatePermission(PermissionNames.ElementStatus_Create, L("Permission_ElementStatus_Create"));
            context.CreatePermission(PermissionNames.ElementStatus_Get, L("Permission_ElementStatus_Get"));
            context.CreatePermission(PermissionNames.ElementStatus_GetList, L("Permission_ElementStatus_GetList"));
            //context.CreatePermission(PermissionNames.ElementStatus_Delete, L("Permission_ElementStatus_Delete"));
            //context.CreatePermission(PermissionNames.ElementStatus_Update, L("Permission_ElementStatus_Update"));

            // EngineElements
            context.CreatePermission(PermissionNames.EngineElement, L("Permission_EngineElement"));
            //context.CreatePermission(PermissionNames.EngineElement_Create, L("Permission_EngineElement_Create"));
            context.CreatePermission(PermissionNames.EngineElement_Get, L("Permission_EngineElement_Get"));
            context.CreatePermission(PermissionNames.EngineElement_GetList, L("Permission_EngineElement_GetList"));
            //context.CreatePermission(PermissionNames.EngineElement_Delete, L("Permission_EngineElement_Delete"));
            //context.CreatePermission(PermissionNames.EngineElement_Update, L("Permission_EngineElement_Update"));

            // Sensors
            context.CreatePermission(PermissionNames.Sensor, L("Permission_Sensor"));
            //context.CreatePermission(PermissionNames.Sensor_Create, L("Permission_Sensor_Create"));
            context.CreatePermission(PermissionNames.Sensor_Get, L("Permission_Sensor_Get"));
            context.CreatePermission(PermissionNames.Sensor_GetList, L("Permission_Sensor_GetList"));
            //context.CreatePermission(PermissionNames.Sensor_Delete, L("Permission_Sensor_Delete"));
            //context.CreatePermission(PermissionNames.Sensor_Update, L("Permission_Sensor_Update"));

            // TestResults
            context.CreatePermission(PermissionNames.TestResult, L("Permission_TestResult"));
            //context.CreatePermission(PermissionNames.TestResult_Create, L("Permission_TestResult_Create"));
            context.CreatePermission(PermissionNames.TestResult_Get, L("Permission_TestResult_Get"));
            context.CreatePermission(PermissionNames.TestResult_GetList, L("Permission_TestResult_GetList"));
            //context.CreatePermission(PermissionNames.TestResult_Delete, L("Permission_TestResult_Delete"));
            //context.CreatePermission(PermissionNames.TestResult_Update, L("Permission_TestResult_Update"));

            // Units
            context.CreatePermission(PermissionNames.Unit, L("Permission_Unit"));
            //context.CreatePermission(PermissionNames.Unit_Create, L("Permission_Unit_Create"));
            context.CreatePermission(PermissionNames.Unit_Get, L("Permission_Unit_Get"));
            context.CreatePermission(PermissionNames.Unit_GetList, L("Permission_Unit_GetList"));
            //context.CreatePermission(PermissionNames.Unit_Delete, L("Permission_Unit_Delete"));
            //context.CreatePermission(PermissionNames.Unit_Update, L("Permission_Unit_Update"));

        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PurificationConsts.LocalizationSourceName);
        }
    }
}
