﻿namespace Purification.Configuration
{
    public static class AppSettingNames
    {
        public const string UiTheme = "App.UiTheme";
    }
}
