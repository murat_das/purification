﻿namespace Purification
{
    public class PurificationConsts
    {
        public const string LocalizationSourceName = "Purification";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
