﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class ElementStatusConfiguration : IEntityTypeConfiguration<ElementStatus>
    {
        public void Configure(EntityTypeBuilder<ElementStatus> builder)
        {
            #region Properties

            builder.ToTable("ElementStatus");

            builder.HasKey(elementStatus => elementStatus.Id);

            builder.Property(elementStatus => elementStatus.Status)
                .HasColumnName("Status");

            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(elementStatus => elementStatus.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}