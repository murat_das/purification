﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class DeviceConfiguration : IEntityTypeConfiguration<Device>
    {
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            #region Properties

            builder.ToTable("Device");

            builder.HasKey(device => device.Id);

            builder.Property(device => device.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<Sensor>(device => device.Sensors)
                .WithOne(sensor => sensor.Device)
                .HasForeignKey(sensor => sensor.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<Entities.EngineElement>(device => device.EngineElements)
                .WithOne(engineElement => engineElement.Device)
                .HasForeignKey(engineElement => engineElement.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(device => device.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}