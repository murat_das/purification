﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class UnitConfiguration : IEntityTypeConfiguration<Unit>
    {
        public void Configure(EntityTypeBuilder<Unit> builder)
        {
            #region Properties

            builder.ToTable("Unit");

            builder.HasKey(unit => unit.Id);

            builder.Property(unit => unit.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<Sensor>(unit => unit.Sensors)
                .WithOne(sensor => sensor.Unit)
                .HasForeignKey(sensor => sensor.UnitId)
                .OnDelete(DeleteBehavior.ClientSetNull);

         

            #endregion Relations

            #region OptimisticLockField
            builder.Property(unit => unit.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}