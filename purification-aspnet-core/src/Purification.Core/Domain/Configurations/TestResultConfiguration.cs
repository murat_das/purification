﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class TestResultConfiguration : IEntityTypeConfiguration<TestResult>
    {
        public void Configure(EntityTypeBuilder<TestResult> builder)
        {
            #region Properties

            builder.ToTable("TestResult");

            builder.HasKey(testResult => testResult.Id);

            builder.Property(testResult => testResult.Value)
                .HasColumnName("Value");

            #endregion Properties

            #region Relations
        

            #endregion Relations

            #region OptimisticLockField
            builder.Property(testResult => testResult.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}