﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class EngineElementConfiguration : IEntityTypeConfiguration<EngineElement>
    {
        public void Configure(EntityTypeBuilder<EngineElement> builder)
        {
            #region Properties

            builder.ToTable("EngineElement");

            builder.HasKey(engineElement => engineElement.Id);

            builder.Property(engineElement => engineElement.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(engineElement => engineElement.DisplayName)
                .HasColumnName("DisplayName")
                .HasMaxLength(150);
            builder.Property(engineElement => engineElement.Status)
                .HasColumnName("Status");


            #endregion Properties

            #region Relations
            builder.HasMany<ElementStatus>(engineElement => engineElement.ElementStatuses)
                .WithOne(elementStatus => elementStatus.EngineElement)
                .HasForeignKey(elementStatus => elementStatus.EngineElementId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            #endregion Relations

            #region OptimisticLockField
            builder.Property(engineElement => engineElement.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}