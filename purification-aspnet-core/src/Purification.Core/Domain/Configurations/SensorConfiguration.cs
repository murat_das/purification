﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Purification.Domain.Entities;

namespace Purification.Domain.Configurations
{
    public class SensorConfiguration : IEntityTypeConfiguration<Sensor>
    {
        public void Configure(EntityTypeBuilder<Sensor> builder)
        {
            #region Properties

            builder.ToTable("Sensor");

            builder.HasKey(sensor => sensor.Id);

            builder.Property(sensor => sensor.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            builder.Property(sensor => sensor.DisplayName)
                .HasColumnName("DisplayName")
                .HasMaxLength(150);

            #endregion Properties

            #region Relations
            builder.HasMany<TestResult>(sensor => sensor.TestResults)
                .WithOne(testResult => testResult.Sensor)
                .HasForeignKey(testResult => testResult.SensorId)
                .OnDelete(DeleteBehavior.ClientSetNull);

         

            #endregion Relations

            #region OptimisticLockField
            builder.Property(sensor => sensor.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}