﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Purification.MultiTenancy;

namespace Purification.Domain.Entities
{
    public class ElementStatus : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public ElementStatus()
        {
            
        }
        #endregion Constructor

        #region Properties
        public byte Status { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int? EngineElementId { get; set; }
        public virtual EngineElement EngineElement { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}