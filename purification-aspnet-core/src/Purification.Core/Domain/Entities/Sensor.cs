﻿using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Purification.MultiTenancy;

namespace Purification.Domain.Entities
{
    public class Sensor : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Sensor()
        {
            TestResults =new HashSet<TestResult>();
        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public string DisplayName { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations

        public int? DeviceId { get; set; }
        public virtual Device Device { get; set; }

        public int? UnitId { get; set; }
        public virtual Unit Unit { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<TestResult> TestResults { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}