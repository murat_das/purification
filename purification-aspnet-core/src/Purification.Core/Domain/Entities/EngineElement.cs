﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Purification.MultiTenancy;

namespace Purification.Domain.Entities
{
    public class EngineElement : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public EngineElement()
        {
            ElementStatuses = new HashSet<ElementStatus>();

        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public string DisplayName { get; set; }
       

        public byte Status { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int? DeviceId { get; set; }
        public virtual Device Device { get; set; }

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<ElementStatus> ElementStatuses { get; set; }


        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}