﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Purification.MultiTenancy;

namespace Purification.Domain.Entities
{
    public class TestResult : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor

        public TestResult()
        {
            
        }
        #endregion Constructor

        #region Properties

        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public decimal Value { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations
        public int? SensorId { get; set; }
        public virtual Sensor Sensor { get; set; }
        #endregion One To One Relations

        #region One To Many Relations



        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

    }
}