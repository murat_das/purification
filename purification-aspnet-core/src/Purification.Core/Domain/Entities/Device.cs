﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Purification.MultiTenancy;

namespace Purification.Domain.Entities
{
    public class Device : FullAuditedEntity<int>, IMustHaveTenant
    {
        #region Constructor
        public Device()
        {
            Sensors = new HashSet<Sensor>();
            EngineElements = new HashSet<EngineElement>();

        }
        #endregion Constructor

        #region Properties

        public string Name { get; set; }
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }

        #endregion Properties   

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations

        public virtual ICollection<Sensor> Sensors { get; set; }
        public virtual ICollection<EngineElement> EngineElements { get; set; }



        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField

        
    }
}