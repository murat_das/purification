﻿using System.Collections.Generic;
using Abp.MultiTenancy;
using Purification.Authorization.Users;
using Purification.Domain.Entities;

namespace Purification.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            Devices = new HashSet<Device>();
            Units = new HashSet<Unit>();
            Sensors = new HashSet<Sensor>();
            TestResults = new HashSet<TestResult>();
            EngineElements = new HashSet<EngineElement>();
            ElementStatuses = new HashSet<ElementStatus>();

        }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
        public virtual ICollection<Sensor> Sensors { get; set; }
        public virtual ICollection<TestResult> TestResults { get; set; }
        public virtual ICollection<EngineElement> EngineElements { get; set; }
        public virtual ICollection<ElementStatus> ElementStatuses { get; set; }



        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
