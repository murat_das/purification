﻿using AutoMapper;
using Purification.Domain.Dashboard.Dtos;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.ElementStatuses.Dtos;
using Purification.Domain.EngineElements.Dtos;
using Purification.Domain.Entities;
using Purification.Domain.Sensors.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.TestResults.Dtos;
using Purification.Domain.Units.Dtos;
using Purification.EntityFrameworkCore.Repositories.Extensions;
using Purification.MultiTenancy;

namespace Purification.Manager
{
    public class MapperManager
    {
        public static void DtosToDomain(IMapperConfigurationExpression cfg) //Dtolu verileri Entitylere göre çevir
        {

            #region Dashboards
            cfg.CreateMap<SensorReportLineChartOutPut, TestResult>();
            cfg.CreateMap<TestResult, SensorReportLineChartOutPut>()
                .ForMember(x => x.Date,
                    y => y.MapFrom(z => z.CreationTime));

            cfg.CreateMap<SensorReportLineChartOneHourOutPut, TestResultOneHourData>();
            cfg.CreateMap<TestResultOneHourData, SensorReportLineChartOneHourOutPut>();



            #endregion

            #region Devices
            cfg.CreateMap<Device, CreateDeviceInput>();
            cfg.CreateMap<Device, GetDeviceInput>();
            cfg.CreateMap<Device, DeleteDeviceInput>();
            cfg.CreateMap<Device, UpdateDeviceInput>();
            cfg.CreateMap<Device, DeviceFullOutPut>();
            cfg.CreateMap<Device, DevicePartOutPut>();
            #endregion

            #region Units
            cfg.CreateMap<Unit, CreateUnitInput>();
            cfg.CreateMap<Unit, GetUnitInput>();
            cfg.CreateMap<Unit, DeleteUnitInput>();
            cfg.CreateMap<Unit, UpdateUnitInput>();
            cfg.CreateMap<Unit, UnitFullOutPut>();
            cfg.CreateMap<Unit, UnitPartOutPut>();
            #endregion

            #region ElementStatuses
            cfg.CreateMap<ElementStatus, CreateElementStatusInput>();
            cfg.CreateMap<ElementStatus, GetElementStatusInput>();
            cfg.CreateMap<ElementStatus, DeleteElementStatusInput>();
            cfg.CreateMap<ElementStatus, UpdateElementStatusInput>();
            cfg.CreateMap<ElementStatus, ElementStatusFullOutPut>();
            #endregion

            #region EngineElements
            cfg.CreateMap<EngineElement, CreateEngineElementInput>();
            cfg.CreateMap<EngineElement, GetEngineElementInput>();
            cfg.CreateMap<EngineElement, DeleteEngineElementInput>();
            cfg.CreateMap<EngineElement, UpdateEngineElementInput>();
            cfg.CreateMap<EngineElement, EngineElementFullOutPut>();
            cfg.CreateMap<EngineElement, EngineElementPartOutPut>();
            #endregion

            #region TestResults
            cfg.CreateMap<TestResult, CreateTestResultInput>();
            cfg.CreateMap<TestResult, GetTestResultInput>();
            cfg.CreateMap<TestResult, DeleteTestResultInput>();
            cfg.CreateMap<TestResult, UpdateTestResultInput>();
            cfg.CreateMap<TestResult, TestResultFullOutPut>();
            #endregion

            #region Sensors
            cfg.CreateMap<Sensor, CreateSensorInput>();
            cfg.CreateMap<Sensor, GetSensorInput>();
            cfg.CreateMap<Sensor, DeleteSensorInput>();
            cfg.CreateMap<Sensor, UpdateSensorInput>();
            cfg.CreateMap<Sensor, SensorFullOutPut>();
            cfg.CreateMap<Sensor, SensorPartOutPut>();
            #endregion

            #region Sensors
            cfg.CreateMap<Tenant, TenantPartOutPut>();
            #endregion
        }
    }
}