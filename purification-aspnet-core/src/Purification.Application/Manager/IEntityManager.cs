﻿using System.Threading.Tasks;
using Abp.Dependency;
using Purification.Domain.Entities;

namespace Purification.Manager
{
    public interface IEntityManager : ITransientDependency
    {
        Task<Device> GetDeviceAsync(int deviceId);
        Task<TestResult> GetTestResultAsync(int testResultId);
        Task<Unit> GetUnitAsync(int unitId);
        Task<Sensor> GetSensorAsync(int sensorId);
        Task<ElementStatus> GetElementStatusAsync(int elementStatusId);
        Task<EngineElement> GetEngineElementAsync(int engineElementId);
    }
}