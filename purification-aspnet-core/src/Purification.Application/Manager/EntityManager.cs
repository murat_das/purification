﻿using System;
using System.Data;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Castle.Core.Logging;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Services;

namespace Purification.Manager
{
    public class EntityManager:AbpAppServiceBase,IEntityManager
    {
        private readonly IDeviceRepository _deviceRepository;
        private readonly IUnitRepository _unitRepository;
        private readonly ISensorRepository _sensorRepository;
        private readonly ITestResultRepository _testResultRepository;
        private readonly IEngineElementRepository _engineElementRepository;
        private readonly IElementStatusRepository _elementStatusRepository;



        public EntityManager(IAbpSession abpSession,
            ILocalizationManager localizationManager,
            ILogger logger,
            IUnitOfWorkManager unitOfWorkManager,
            IObjectMapper objectMapper, IDeviceRepository deviceRepository, IUnitRepository unitRepository, ISensorRepository sensorRepository, ITestResultRepository testResultRepository, IEngineElementRepository engineElementRepository, IElementStatusRepository elementStatusRepository) 
            : base(abpSession, localizationManager, logger, unitOfWorkManager, objectMapper)
        {
            _deviceRepository = deviceRepository;
            _unitRepository = unitRepository;
            _sensorRepository = sensorRepository;
            _testResultRepository = testResultRepository;
            _engineElementRepository = engineElementRepository;
            _elementStatusRepository = elementStatusRepository;
        }

        public async Task<Device> GetDeviceAsync(int deviceId)
        {
            var device = await _deviceRepository.FirstOrDefaultAsync(x =>
                x.Id == deviceId
            );
            if (device == null)
            {
                throw new EntityNotFoundException(typeof(Device), deviceId);
            }

            return device;
        }

        public async Task<ElementStatus> GetElementStatusAsync(int elementStatusId)
        {
            var elementStatus = await _elementStatusRepository.FirstOrDefaultAsync(x =>
                x.Id == elementStatusId
            );
            if (elementStatus == null)
            {
                throw new EntityNotFoundException(typeof(ElementStatus), elementStatusId);
            }

            return elementStatus;
        }

        public async Task<EngineElement> GetEngineElementAsync(int engineElementId)
        {
            var engineElement = await _engineElementRepository.FirstOrDefaultAsync(x =>
                x.Id == engineElementId
            );
            if (engineElement == null)
            {
                throw new EntityNotFoundException(typeof(EngineElement), engineElementId);
            }

            return engineElement;
        }

        public async Task<Sensor> GetSensorAsync(int sensorId)
        {
            var sensor = await _sensorRepository.FirstOrDefaultAsync(x =>
                x.Id == sensorId
            );
            if (sensor == null)
            {
                throw new EntityNotFoundException(typeof(Sensor), sensorId);
            }

            return sensor;
        }

        public async Task<TestResult> GetTestResultAsync(int testResultId)
        {
            var testResult = await _testResultRepository.FirstOrDefaultAsync(x =>
                x.Id == testResultId
            );
            if (testResult == null)
            {
                throw new EntityNotFoundException(typeof(TestResult), testResultId);
            }

            return testResult;
        }

        public async Task<Unit> GetUnitAsync(int unitId)
        {
            var unit = await _unitRepository.FirstOrDefaultAsync(x =>
                x.Id == unitId
            );
            if (unit == null)
            {
                throw new EntityNotFoundException(typeof(Unit), unitId);
            }

            return unit;
        }
    }
}