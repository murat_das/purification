﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.EngineElements.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.EngineElements
{
    [AbpAuthorize(PermissionNames.EngineElement)]
    public class EngineElementAppService : PurificationAppServiceBase, IEngineElementAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IEngineElementRepository _engineElementRepository;

        public EngineElementAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IEngineElementRepository engineElementRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _engineElementRepository = engineElementRepository;
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.EngineElement_Create)]
        public async Task<EngineElementFullOutPut> CreateAsync(CreateEngineElementInput input)
        {
            var engineElement = new EngineElement()
            {
                TenantId = input.Device.Tenant.Id,
                Name = input.Name,
                DisplayName = input.DisplayName,
                Status = input.Status,
                DeviceId = input.Device?.Id
            };

            await _engineElementRepository.InsertAsync(engineElement);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<EngineElementFullOutPut>(engineElement);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.EngineElement_Get)]
        public async Task<EngineElementFullOutPut> GetAsync(GetEngineElementInput input)
        {
            var engineElement = await _entityManager.GetEngineElementAsync(input.Id);

            return ObjectMapper.Map<EngineElementFullOutPut>(engineElement);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.EngineElement_GetList)]
        public async Task<List<EngineElementFullOutPut>> GetListAsync()
        {

            var engineElementList = await _engineElementRepository.GetAllListAsync();

            return ObjectMapper.Map<List<EngineElementFullOutPut>>(engineElementList);
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.EngineElement_Delete)]
        public async Task DeleteAsync(DeleteEngineElementInput input)
        {
            var engineElement = await _entityManager.GetEngineElementAsync(input.Id);

            await _engineElementRepository.DeleteAsync(engineElement.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.EngineElement_Update)]
        public async Task<EngineElementFullOutPut> UpdateAsync(UpdateEngineElementInput input)
        {
            var engineElement = await _entityManager.GetEngineElementAsync(input.Id);

            engineElement.Name = input.Name.IsNullOrEmpty() ? engineElement.Name : input.Name;
            engineElement.DisplayName = input.DisplayName.IsNullOrEmpty() ? engineElement.Name : input.DisplayName;
            engineElement.Status = input.Status;
            engineElement.DeviceId = input.Device?.Id;
            engineElement.TenantId = input.Device.Tenant.Id;

            await _engineElementRepository.UpdateAsync(engineElement);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<EngineElementFullOutPut>(engineElement);
        }
    }
}