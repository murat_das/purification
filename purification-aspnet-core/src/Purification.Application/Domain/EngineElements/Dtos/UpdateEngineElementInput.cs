﻿using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;

namespace Purification.Domain.EngineElements.Dtos
{
    public class UpdateEngineElementInput:EntityDto<int>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public byte Status { get; set; }
        public DevicePartOutPut Device { get; set; }
    }
}