﻿using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.EngineElements.Dtos
{
    public class EngineElementPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public TenantPartOutPut Tenant { get; set; }

    }
}