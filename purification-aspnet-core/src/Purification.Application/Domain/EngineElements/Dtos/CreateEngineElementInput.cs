﻿using Purification.Domain.Devices.Dtos;

namespace Purification.Domain.EngineElements.Dtos
{
    public class CreateEngineElementInput
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public byte Status { get; set; }
        public DevicePartOutPut Device { get; set; }
    }
}