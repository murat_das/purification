﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.ElementStatuses.Dtos;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.EngineElements.Dtos
{
    public class EngineElementFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public byte Status { get; set; }
        public TenantPartOutPut Tenant { get; set; }
        public DevicePartOutPut Device { get; set; }
        public List<ElementStatusFullOutPut> ElementStatuses { get; set; }

    }
}