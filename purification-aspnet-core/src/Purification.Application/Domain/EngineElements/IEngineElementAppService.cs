﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.EngineElements.Dtos;

namespace Purification.Domain.EngineElements
{
    public interface IEngineElementAppService : IApplicationService
    {
        #region Async Methods
        Task<EngineElementFullOutPut> CreateAsync(CreateEngineElementInput input);
        Task<EngineElementFullOutPut> GetAsync(GetEngineElementInput input);
        Task<List<EngineElementFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteEngineElementInput input);
        Task<EngineElementFullOutPut> UpdateAsync(UpdateEngineElementInput input);
        #endregion Async Methods
    }
}