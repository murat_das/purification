﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Tenants
{
    public interface ITenantAppService : IApplicationService
    {
        #region Async Methods
        Task<List<TenantPartOutPut>> GetListAsync();
        Task UploadImageAsync(object file);

        
        #endregion Async Methods
    }
}