using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using Purification.MultiTenancy;

namespace Purification.Domain.Tenants.Dtos
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantPartOutPut : EntityDto<int>
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        [RegularExpression(AbpTenantBase.TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string Name { get; set; }      
    }
}
