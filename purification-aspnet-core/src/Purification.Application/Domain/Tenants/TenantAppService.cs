﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Castle.Core.Internal;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;
using Purification.MultiTenancy;

namespace Purification.Domain.Tenants
{
    //[AbpAuthorize(PermissionNames.Tenant)]
    public class TenantAppService : PurificationAppServiceBase, ITenantAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITenantRepository _tenantRepository;

        public TenantAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITenantRepository tenantRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _tenantRepository = tenantRepository;
        }
        
        [HttpPost]
        public async Task<List<TenantPartOutPut>> GetListAsync()
        {

            var tenantList = await _tenantRepository.GetAllListAsync();

            return ObjectMapper.Map<List<TenantPartOutPut>>(tenantList);
        }
        [HttpPost]
        [DisableValidation]
        public async Task UploadImageAsync(object file)
        {


        }
    }
}