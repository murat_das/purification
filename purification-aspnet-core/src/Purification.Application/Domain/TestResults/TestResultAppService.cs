﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.TestResults.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.TestResults
{
    [AbpAuthorize(PermissionNames.TestResult)]
    public class TestResultAppService : PurificationAppServiceBase, ITestResultAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITestResultRepository _testResultRepository;

        public TestResultAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITestResultRepository testResultRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _testResultRepository = testResultRepository;
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.TestResult_Create)]
        public async Task<TestResultFullOutPut> CreateAsync(CreateTestResultInput input)
        {
            var testResult = new TestResult()
            {
                TenantId = input.Sensor.Tenant.Id,
               Value = input.Value,
               SensorId = input.Sensor?.Id
            };

            await _testResultRepository.InsertAsync(testResult);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_Get)]
        public async Task<TestResultFullOutPut> GetAsync(GetTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.TestResult_GetList)]
        public async Task<List<TestResultFullOutPut>> GetListAsync()
        {

            var testResultList = await _testResultRepository.GetAllListAsync();

            return ObjectMapper.Map<List<TestResultFullOutPut>>(testResultList);
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.TestResult_Delete)]
        public async Task DeleteAsync(DeleteTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            await _testResultRepository.DeleteAsync(testResult.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.TestResult_Update)]
        public async Task<TestResultFullOutPut> UpdateAsync(UpdateTestResultInput input)
        {
            var testResult = await _entityManager.GetTestResultAsync(input.Id);

            testResult.Value = input.Value;
            testResult.SensorId = input.Sensor?.Id;

            await _testResultRepository.UpdateAsync(testResult);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TestResultFullOutPut>(testResult);
        }
    }
}