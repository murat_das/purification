﻿using Purification.Domain.Sensors.Dtos;

namespace Purification.Domain.TestResults.Dtos
{
    public class CreateTestResultInput
    {
        public decimal Value { get; set; }

        public SensorPartOutPut Sensor { get; set; }
    }
}