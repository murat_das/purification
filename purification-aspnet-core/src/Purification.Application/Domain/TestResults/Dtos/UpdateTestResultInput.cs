﻿using Abp.Application.Services.Dto;
using Purification.Domain.Sensors.Dtos;

namespace Purification.Domain.TestResults.Dtos
{
    public class UpdateTestResultInput : EntityDto<int>
    {
        public decimal Value { get; set; }

        public SensorPartOutPut Sensor { get; set; }
    }
}