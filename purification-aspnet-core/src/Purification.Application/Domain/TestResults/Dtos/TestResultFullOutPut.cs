﻿using System;
using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Sensors.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.Units.Dtos;

namespace Purification.Domain.TestResults.Dtos
{
    public class TestResultFullOutPut : EntityDto<int>
    {
        public decimal Value { get; set; }
        public DateTime CreationTime { get; set; }
        public TenantPartOutPut Tenant { get; set; }

        public SensorPartOutPut Sensor { get; set; }

    }
}