﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.Dashboard.Dtos;
using Purification.Domain.Sensors.Dtos;

namespace Purification.Domain.Dashboard
{
    public interface IDashboardAppService : IApplicationService
    {
        #region Async Methods
        Task<List<SensorReportLineChartOutPut>> GetSensorLineChartListAsync(GetSensorInput input);
         Task<List<SensorReportLineChartOutPut>> GetSensorLineChartOneHourListAsync(GetSensorInput input);
         Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneDayListAsync(GetSensorInput input);
         Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneWeekListAsync(GetSensorInput input);
         Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneMountListAsync(GetSensorInput input);
         Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneYearListAsync(GetSensorInput input);

        Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartSpecialListAsync(GetSensorInput input);


        #endregion
    }
}