﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Purification.Authorization;
using Purification.Domain.Dashboard;
using Purification.Domain.Dashboard.Dtos;
using Purification.Domain.Entities;
using Purification.Domain.Sensors.Dtos;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.Dashboards
{
    //[AbpAuthorize(PermissionNames.Dashboard)]
    public class DashboardAppService : PurificationAppServiceBase, IDashboardAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITestResultRepository _testResultRepository;

        //private readonly IDashboardRepository _dashboardRepository;

        public DashboardAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITestResultRepository testResultRepository
            //IDashboardRepository dashboardRepository
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _testResultRepository = testResultRepository;
            //_dashboardRepository = dashboardRepository;
        }

        [HttpPost]
        //[AbpAuthorize(PermissionNames.Dashboard_GetList)]
        public async Task<List<SensorReportLineChartOutPut>> GetSensorLineChartListAsync(GetSensorInput input)
        {
            var dashboardList = _testResultRepository.Query(q => q.Where(x => x.SensorId == input.Id).Take(100)).ToList();
            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOutPut>>(dashboardList);
            return sensorReportLineChartOutPut;
        }
      
        [HttpPost]
        public async Task<List<SensorReportLineChartOutPut>> GetSensorLineChartOneHourListAsync(GetSensorInput input)
        {
            var dashboardList = _testResultRepository.Query(q => q.Where(x => x.SensorId == input.Id && x.CreationTime >= DateTime.Now.AddHours(-1))).ToList();
            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOutPut>>(dashboardList);
            return sensorReportLineChartOutPut;
        }
    
        [HttpPost]
        public async Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneDayListAsync(GetSensorInput input)
        {

            //var dashboardList = _testResultRepository.Query(q => q.Where(x => x.SensorId == input.Id && x.CreationTime >= DateTime.Now.AddDays(-1))).ToList();
            //var groupedItems = dashboardList.GroupBy(x => new { Hour = x.CreationTime.Hour })
            //    .Select(s => new SensorReportLineChartOneHourOutPut()
            //    {
            //        Date = s.Key.Hour.ToString(),
            //        Value = s.Where(x=>x.Value>0).Average(x => x.Value)
            //    }).ToList();

            var testResultOneHourDataList = await _testResultRepository.GetOneDayTestResult(input.Id);

            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOneHourOutPut>>(testResultOneHourDataList);
            return sensorReportLineChartOutPut;
        }
    
        [HttpPost]
        public async Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneWeekListAsync(GetSensorInput input)
        {
            var testResultOneHourDataList = await _testResultRepository.GetOneWeekTestResult(input.Id);

            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOneHourOutPut>>(testResultOneHourDataList);
            return sensorReportLineChartOutPut;
        }
      
        [HttpPost]
        public async Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneMountListAsync(GetSensorInput input)
        {
            var testResultOneHourDataList = await _testResultRepository.GetOneMountTestResult(input.Id);

            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOneHourOutPut>>(testResultOneHourDataList);
            return sensorReportLineChartOutPut;
        }

        [HttpPost]
        public async Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartOneYearListAsync(GetSensorInput input)
        {
            var testResultOneHourDataList = await _testResultRepository.GetOneYearTestResult(input.Id);

            var sensorReportLineChartOutPut = ObjectMapper.Map<List<SensorReportLineChartOneHourOutPut>>(testResultOneHourDataList);
            return sensorReportLineChartOutPut;
        }

        [HttpPost]
        public async Task<List<SensorReportLineChartOneHourOutPut>> GetSensorLineChartSpecialListAsync(GetSensorInput input)
        {
            throw new System.NotImplementedException();
        }
    }
}