﻿using System;
using Abp.Application.Services.Dto;

namespace Purification.Domain.Dashboard.Dtos
{
    public class SensorReportLineChartOutPut : EntityDto<int>
    {

        private string date;
        public string Date
        {
            get => date;
            set =>
                date =Convert.ToDateTime(value).ToString("dd/MM/yyyy HH:mm:ss");
        }
        public decimal Value { get; set; }
    }
}