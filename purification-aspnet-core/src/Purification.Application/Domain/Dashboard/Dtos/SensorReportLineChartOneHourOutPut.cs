﻿using System;
using Abp.Application.Services.Dto;

namespace Purification.Domain.Dashboard.Dtos
{
    public class SensorReportLineChartOneHourOutPut : EntityDto<int>
    {

        public string Date { get; set; }
      
        public decimal Value { get; set; }
    }
}