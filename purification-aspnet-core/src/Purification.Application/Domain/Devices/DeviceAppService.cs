﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.Devices
{
    [AbpAuthorize(PermissionNames.Device)]
    public class DeviceAppService : PurificationAppServiceBase, IDeviceAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceRepository _deviceRepository;

        public DeviceAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceRepository deviceRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceRepository = deviceRepository;
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Device_Create)]
        public async Task<DeviceFullOutPut> CreateAsync(CreateDeviceInput input)
        {
            var device = new Device()
            {
                Name = input.Name,
                TenantId = input.Tenant.Id
            };

            await _deviceRepository.InsertAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_Get)]
        public async Task<DeviceFullOutPut> GetAsync(GetDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }

        //[HttpPost]
        //[AbpAuthorize(PermissionNames.Device_GetList)]
        //public async Task<List<DeviceFullOutPut>> GetDeviceTenantListAsync()
        //{

        //    var deviceList = await _deviceRepository.GetAllListAsync();

        //    return ObjectMapper.Map<List<DeviceFullOutPut>>(deviceList);
        //}

        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_GetList)]
        public async Task<List<DeviceFullOutPut>> GetListAsync()
        {

            var deviceList = await _deviceRepository.GetAllListAsync();

            return ObjectMapper.Map<List<DeviceFullOutPut>>(deviceList);
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Device_Delete)]
        public async Task DeleteAsync(DeleteDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            await _deviceRepository.DeleteAsync(device.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Device_Update)]
        public async Task<DeviceFullOutPut> UpdateAsync(UpdateDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            device.Name = input.Name.IsNullOrEmpty() ? device.Name : input.Name;

            await _deviceRepository.UpdateAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
    }
}