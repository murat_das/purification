﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.Devices.Dtos;

namespace Purification.Domain.Devices
{
    public interface IDeviceAppService : IApplicationService
    {
        #region Async Methods
        Task<DeviceFullOutPut> CreateAsync(CreateDeviceInput input);
        Task<DeviceFullOutPut> GetAsync(GetDeviceInput input);
        //Task<List<DeviceFullOutPut>> GetDeviceTenantListAsync();
        Task<List<DeviceFullOutPut>> GetListAsync();

        Task DeleteAsync(DeleteDeviceInput input);
        Task<DeviceFullOutPut> UpdateAsync(UpdateDeviceInput input);
        #endregion Async Methods
    }
}