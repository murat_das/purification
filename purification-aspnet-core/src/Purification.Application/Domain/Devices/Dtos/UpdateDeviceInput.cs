﻿using Abp.Application.Services.Dto;

namespace Purification.Domain.Devices.Dtos
{
    public class UpdateDeviceInput : EntityDto<int>
    {
        public string Name { get; set; }
    }
}