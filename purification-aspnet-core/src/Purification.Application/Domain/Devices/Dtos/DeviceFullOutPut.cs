﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Purification.Domain.Entities;
using Purification.Domain.Sensors.Dtos;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Devices.Dtos
{
    public class DeviceFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }

        public TenantPartOutPut Tenant { get; set; }
        public List<SensorPartOutPut> Sensors { get; set; }
    }
}