﻿using Abp.Application.Services.Dto;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Devices.Dtos
{
    public class DevicePartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public TenantPartOutPut Tenant { get; set; }

    }
}