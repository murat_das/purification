﻿using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Devices.Dtos
{
    public class CreateDeviceInput
    {
        public string Name { get; set; }
        public TenantPartOutPut Tenant { get; set; }
    }
}