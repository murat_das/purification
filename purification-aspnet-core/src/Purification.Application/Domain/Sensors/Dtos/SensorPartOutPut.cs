﻿using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.Units.Dtos;

namespace Purification.Domain.Sensors.Dtos
{
    public class SensorPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public TenantPartOutPut Tenant { get; set; }
        public UnitPartOutPut Unit { get; set; }
        //public DevicePartOutPut Device { get; set; }

    }
}