﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.TestResults.Dtos;
using Purification.Domain.Units.Dtos;

namespace Purification.Domain.Sensors.Dtos
{
    public class SensorFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public UnitPartOutPut Unit { get; set; }
        public TenantPartOutPut Tenant { get; set; }

        public DevicePartOutPut Device { get; set; }
        //public List<TestResultFullOutPut> TestResults { get; set; }
    }
}