﻿using Purification.Domain.Devices.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.Units.Dtos;

namespace Purification.Domain.Sensors.Dtos
{
    public class CreateSensorInput
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public DevicePartOutPut Device { get; set; }
        public UnitPartOutPut Unit { get; set; }

    }
}