﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.Sensors.Dtos;

namespace Purification.Domain.Sensors
{
    public interface ISensorAppService : IApplicationService
    {
        #region Async Methods
        Task<SensorFullOutPut> CreateAsync(CreateSensorInput input);
        Task<SensorFullOutPut> GetAsync(GetSensorInput input);
        Task<List<SensorFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteSensorInput input);
        Task<SensorFullOutPut> UpdateAsync(UpdateSensorInput input);
        #endregion Async Methods
    }
}