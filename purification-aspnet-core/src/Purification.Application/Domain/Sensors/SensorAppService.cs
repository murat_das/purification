﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.Sensors.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.Sensors
{
    [AbpAuthorize(PermissionNames.Sensor)]
    public class SensorAppService : PurificationAppServiceBase, ISensorAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ISensorRepository _sensorRepository;

        public SensorAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ISensorRepository sensorRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _sensorRepository = sensorRepository;
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Sensor_Create)]
        public async Task<SensorFullOutPut> CreateAsync(CreateSensorInput input)
        {
            var sensor = new Sensor()
            {
                Name = input.Name,
                DisplayName = input.DisplayName,
                UnitId = input.Unit?.Id,
                DeviceId = input.Device?.Id,
                TenantId = input.Device.Tenant.Id
            };

            await _sensorRepository.InsertAsync(sensor);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<SensorFullOutPut>(sensor);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Sensor_Get)]
        public async Task<SensorFullOutPut> GetAsync(GetSensorInput input)
        {
            var sensor = await _entityManager.GetSensorAsync(input.Id);

            return ObjectMapper.Map<SensorFullOutPut>(sensor);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Sensor_GetList)]
        public async Task<List<SensorFullOutPut>> GetListAsync()
        {

            var sensorList = await _sensorRepository.GetAllListAsync();

            return ObjectMapper.Map<List<SensorFullOutPut>>(sensorList);
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Sensor_Delete)]
        public async Task DeleteAsync(DeleteSensorInput input)
        {
            var sensor = await _entityManager.GetSensorAsync(input.Id);

            await _sensorRepository.DeleteAsync(sensor.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.Sensor_Update)]
        public async Task<SensorFullOutPut> UpdateAsync(UpdateSensorInput input)
        {
            var sensor = await _entityManager.GetSensorAsync(input.Id);

            sensor.Name = input.Name.IsNullOrEmpty() ? sensor.Name : input.Name;
            sensor.DisplayName = input.DisplayName.IsNullOrEmpty() ? sensor.Name : input.DisplayName;
            sensor.UnitId = input.Unit?.Id;
            sensor.DeviceId = input.Device==null?sensor.DeviceId:input.Device.Id;

            await _sensorRepository.UpdateAsync(sensor);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<SensorFullOutPut>(sensor);
        }
    }
}