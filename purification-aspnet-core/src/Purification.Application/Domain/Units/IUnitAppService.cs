﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.Devices.Dtos;
using Purification.Domain.Tenants.Dtos;
using Purification.Domain.Units.Dtos;

namespace Purification.Domain.Units
{
    public interface IUnitAppService : IApplicationService
    {
        #region Async Methods
        Task<UnitFullOutPut> CreateAsync(CreateUnitInput input);
        Task<UnitFullOutPut> GetAsync(GetUnitInput input);
        Task<List<UnitFullOutPut>> GetUnitSelectDeviceListAsync(DeviceFullOutPut input);
        Task<List<UnitFullOutPut>> GetTenantUnitListAsync(TenantPartOutPut input);
        
        Task<List<UnitFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteUnitInput input);
        Task<UnitFullOutPut> UpdateAsync(UpdateUnitInput input);
        #endregion Async Methods
    }
}