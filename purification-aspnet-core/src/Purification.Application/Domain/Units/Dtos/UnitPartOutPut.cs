﻿using Abp.Application.Services.Dto;

namespace Purification.Domain.Units.Dtos
{
    public class UnitPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }

    }
}