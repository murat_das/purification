﻿using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Units.Dtos
{
    public class CreateUnitInput
    {
        public string Name { get; set; }
        public TenantPartOutPut Tenant { get; set; }

    }
}