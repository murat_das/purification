﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Purification.Domain.Sensors.Dtos;
using Purification.Domain.Tenants.Dtos;

namespace Purification.Domain.Units.Dtos
{
    public class UnitFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public TenantPartOutPut Tenant { get; set; }

        public List<SensorPartOutPut> Sensors { get; set; }
    }
}