﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Domain.ElementStatuses.Dtos;

namespace Purification.Domain.ElementStatuses
{
    public interface IElementStatusAppService : IApplicationService
    {
        #region Async Methods
        Task<ElementStatusFullOutPut> CreateAsync(CreateElementStatusInput input);
        Task<ElementStatusFullOutPut> GetAsync(GetElementStatusInput input);
        Task<List<ElementStatusFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteElementStatusInput input);
        Task<ElementStatusFullOutPut> UpdateAsync(UpdateElementStatusInput input);
        #endregion Async Methods
    }
}