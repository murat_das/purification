﻿using System;
using Abp.Application.Services.Dto;
using Purification.Domain.EngineElements.Dtos;

namespace Purification.Domain.ElementStatuses.Dtos
{
    public class ElementStatusFullOutPut:EntityDto<int>
    {
        public DateTime CreationTime { get; set; }
        public byte Status { get; set; }
        public EngineElementPartOutPut EngineElement { get; set; }
    }
}