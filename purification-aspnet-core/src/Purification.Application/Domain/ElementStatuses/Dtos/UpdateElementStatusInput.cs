﻿using Abp.Application.Services.Dto;
using Purification.Domain.EngineElements.Dtos;

namespace Purification.Domain.ElementStatuses.Dtos
{
    public class UpdateElementStatusInput:EntityDto<int>
    {
        public byte Status { get; set; }
        public EngineElementPartOutPut EngineElement { get; set; }
    }
}