﻿using Purification.Domain.EngineElements.Dtos;

namespace Purification.Domain.ElementStatuses.Dtos
{
    public class CreateElementStatusInput
    {
        public byte Status { get; set; }
        public EngineElementPartOutPut EngineElement { get; set; }
    }
}