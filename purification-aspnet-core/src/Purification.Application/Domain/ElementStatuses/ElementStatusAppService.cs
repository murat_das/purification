﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Castle.Core.Internal;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Purification.Authorization;
using Purification.Domain.ElementStatuses.Dtos;
using Purification.Domain.Entities;
using Purification.EntityFrameworkCore.Repositories;
using Purification.Manager;

namespace Purification.Domain.ElementStatuses
{
    [AbpAuthorize(PermissionNames.ElementStatus)]
    public class ElementStatusAppService : PurificationAppServiceBase, IElementStatusAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IElementStatusRepository _elementStatusRepository;

        public ElementStatusAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IElementStatusRepository elementStatusRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _elementStatusRepository = elementStatusRepository;
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.ElementStatus_Create)]
        public async Task<ElementStatusFullOutPut> CreateAsync(CreateElementStatusInput input)
        {
            var elementStatus = new ElementStatus()
            {
                TenantId = input.EngineElement.Tenant.Id,
                Status = input.Status,
                EngineElementId=input.EngineElement?.Id
            };

            await _elementStatusRepository.InsertAsync(elementStatus);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ElementStatusFullOutPut>(elementStatus);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ElementStatus_Get)]
        public async Task<ElementStatusFullOutPut> GetAsync(GetElementStatusInput input)
        {
            var elementStatus = await _entityManager.GetElementStatusAsync(input.Id);

            return ObjectMapper.Map<ElementStatusFullOutPut>(elementStatus);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.ElementStatus_GetList)]
        public async Task<List<ElementStatusFullOutPut>> GetListAsync()
        {

            var elementStatusList = await _elementStatusRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ElementStatusFullOutPut>>(elementStatusList);
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.ElementStatus_Delete)]
        public async Task DeleteAsync(DeleteElementStatusInput input)
        {
            var elementStatus = await _entityManager.GetElementStatusAsync(input.Id);

            await _elementStatusRepository.DeleteAsync(elementStatus.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        //[AbpAuthorize(PermissionNames.ElementStatus_Update)]
        public async Task<ElementStatusFullOutPut> UpdateAsync(UpdateElementStatusInput input)
        {
            var elementStatus = await _entityManager.GetElementStatusAsync(input.Id);

            elementStatus.Status = input.Status;
            elementStatus.EngineElementId = input.EngineElement?.Id;

            await _elementStatusRepository.UpdateAsync(elementStatus);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ElementStatusFullOutPut>(elementStatus);
        }
    }
}