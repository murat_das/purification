﻿using System.Threading.Tasks;
using Purification.Configuration.Dto;

namespace Purification.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
