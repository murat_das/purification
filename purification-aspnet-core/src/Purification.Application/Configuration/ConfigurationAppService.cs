﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Purification.Configuration.Dto;

namespace Purification.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : PurificationAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
