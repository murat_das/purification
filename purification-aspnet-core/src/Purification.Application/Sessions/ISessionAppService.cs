﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Purification.Sessions.Dto;

namespace Purification.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
