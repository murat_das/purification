﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Purification.Authorization;
using Purification.Manager;

namespace Purification
{
    [DependsOn(
        typeof(PurificationCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class PurificationApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<PurificationAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(PurificationApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                cfg =>
                {
                    MapperManager.DtosToDomain(cfg);

                    // Scan the assembly for classes which inherit from AutoMapper.Profile

                    cfg.AddMaps(thisAssembly);
                });
        }
    }
}
