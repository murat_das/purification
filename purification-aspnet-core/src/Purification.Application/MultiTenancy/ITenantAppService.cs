﻿using Abp.Application.Services;
using Purification.MultiTenancy.Dto;

namespace Purification.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

