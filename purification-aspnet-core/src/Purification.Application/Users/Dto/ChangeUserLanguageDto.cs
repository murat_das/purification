using System.ComponentModel.DataAnnotations;

namespace Purification.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}